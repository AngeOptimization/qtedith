#include "value.h"
#include <QSharedData>
#include <QByteArray>

using namespace QtEdith;

class QtEdith::ValueData : public QSharedData {
    public:
        ValueData(QByteArray data) : m_data(data) {
        }
        QByteArray m_data;
        bool operator==(const QtEdith::ValueData& other) const {
            return m_data == other.m_data;
        }
};

QtEdith::Value QtEdith::Value::valueOf(const QByteArray& array) {
    if(array.isEmpty()) {
        return Value();
    }
    return QtEdith::Value(array);
}

Value::Value() : d(0) {

}

double Value::asDouble(bool* ok) const {
    if(d)  {
        return d->m_data.toDouble(ok);
    } else {
        if (ok) {
            *ok = false;
        }
        return 0;
    }
}

double Value::asDouble(double defaultDouble, bool* ok) const {
    if(ok) {
        double result = asDouble(ok);
        if(*ok) {
            return result;
        } else {
            return defaultDouble;
        }
    } else {
        bool tmpOk;
        double result = asDouble(&tmpOk);
        if(tmpOk) {
            return result;
        } else {
            return defaultDouble;
        }
    }
}

QByteArray Value::asString(bool* ok) const {
    if(d) {
        if(ok) {
            *ok = true;
        }
        return d->m_data;
    } else {
        if(ok) {
            *ok = false;
        }
        return QByteArray();
    }
}


QByteArray Value::asString(QByteArray defaultString, bool* ok) const {
    if(ok) {
        QByteArray result = asString(ok);
        if(*ok) {
            return result;
        } else {
            return defaultString;
        }
    } else {
        bool tmpOk;
        QByteArray result = asString(&tmpOk);
        if(tmpOk) {
            return result;
        } else {
            return defaultString;
        }
    }
}

int Value::asInt(int defaultInt, bool* ok) const {
    bool tmpOk;
    if (!ok) {
        ok = &tmpOk;
    }
    if (!d) {
        *ok = false;
        return defaultInt;
    }
    int result = d->m_data.toInt(ok);
    if (*ok) {
        return result;
    } else {
        return defaultInt;
    }
}

int Value::asInt(bool* ok) const {
    return asInt(0, ok);
}

bool Value::isPresent() const {
    return d != 0;
}

Value::~Value() {
  // empty
}

Value& Value::operator=(const Value& other) {
    this->d = other.d;
    return *this;
}

bool Value::operator==(const Value& other) const {
    if(this->d == other.d) {
        return true;
    }
    if(this->d && other.d) {
        return *(this->d) == *(other.d);
    }
    return false;
}

bool Value::operator!=(const Value& other) const {
    return ! (*this == other);
}


Value::Value(const QByteArray& array) : d(new QtEdith::ValueData(array)) {
}

Value::Value(const Value& other) : d(other.d) {

}






