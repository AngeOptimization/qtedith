#ifndef QTEDITH_PREDEFINEDFORMATS_H
#define QTEDITH_PREDEFINEDFORMATS_H

#include "transitiontable.h"
#include "qtedith_export.h"

namespace QtEdith {

/**
 * Creates various TransitionTables for some pre defined formats
 */
class QTEDITH_EXPORT PredefinedFormats {
    public:
        /**
         * Creats a transition table for a Baplie98B
         */
        static QtEdith::TransitionTable createBaplie95BTransitionTable();
        /**
         * Implements grouping of COPRAR based on D00B as defined in
         *
         * SMDG User Manual ( Implementation Guide )
         * COPRAR (Container discharge/loading order)
         * Version 2.0
         * UN/EDIFACT D00B
         */
        static QtEdith::TransitionTable createCoprar00BTransitionTable();
};
}

#endif // QTEDITH_PREDEFINEDFORMATS_H
