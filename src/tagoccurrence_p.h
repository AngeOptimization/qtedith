#ifndef TAGOCCURANCE_P_H
#define TAGOCCURANCE_P_H
// internal helper class - just a glorified pair.
#include <QByteArray>

namespace QtEdithPrivate {

typedef QPair<QByteArray, int> TagOccurence;

}
#endif // TAGOCCURANCE_P_H

