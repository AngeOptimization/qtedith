#include "edifactsegmentreader.h"
#include "edifacttokenreader.h"
#include "edifactsegmentreader.h"
#include <QByteArray>

using namespace QtEdith;

class QtEdith::EdifactSegmentReaderPrivate {
public:
    EdifactSegmentReaderPrivate(QIODevice* device)
      : m_scanner(device), m_nextIsValid(false), m_error(0), m_firstSegment(true)
    {
        // Empty
    }

    void ensureNextIsValid() {
        if (m_nextIsValid) {
            return;
        }
        if (m_firstSegment) {
            m_firstSegment = false;
            consumePossibleUnaSegment(); // This might change m_error to != 0
        }
        if (m_error != 0) {
            m_next = Segment();
            m_nextIsValid = true;
            return;
        }
        // XXX The error code from the scanner is generally ignored in the parsing below, this might mean that scanner
        //     errors might be signaled by the segment reader in the segment after the one with the error
        if (m_scanner.errorCode() != 0) {
            m_error = m_scanner.errorCode();
            m_nextIsValid = true;
            m_next = Segment();
            return;
        }

        // Read Tag
        if (!m_scanner.hasNext()) {
            m_next = Segment();
            m_nextIsValid = true;
            return;
        }
        Q_ASSERT(m_scanner.hasNext());
        Token tag = m_scanner.next();
        if (tag.type() != Token::Value) {
            m_error = 2;
            m_nextIsValid = true;
            m_next = Segment();
            return;
        }
        SegmentBuilder builder(tag.value());

        // Read separatorAfterTag
        if (!m_scanner.hasNext()) {
            m_error = 11;
            m_next = Segment();
            m_nextIsValid = true;
            return;
        }
        Q_ASSERT(m_scanner.hasNext());
        Token separatorAfterTag = m_scanner.next();
        if (separatorAfterTag.type() != Token::DataElementSeparator) {
            m_error = 3;
            m_nextIsValid = true;
            m_next = Segment();
            return;
        }

        // Read the rest
        bool readyForValue = true;
        while (m_scanner.hasNext()) {
            Q_ASSERT(m_scanner.hasNext());
            Token token = m_scanner.next();
            switch (token.type()) {
                case Token::Value: {
                    builder.addValue(token.value());
                    readyForValue = false;
                    break;
                }
                case Token::DataElementSeparator: {
                    builder.nextDataElement();
                    readyForValue = true;
                    break;
                }
                case Token::ComponentDataElementSeparator: {
                    if(readyForValue) {
                        builder.addValue(QByteArray());
                    }
                    readyForValue=true;
                    break;
                }
                case Token::SegmentTerminator: {
                    m_next = builder.build();
                    m_nextIsValid = true;
                    return; // Success
                }
            }
        }

        // Encounteren EOF or scanner error while reading
        m_error = 4;
        m_next = Segment();
        m_nextIsValid = true;
        return;
    }

    void consumePossibleUnaSegment() {
        if (!m_scanner.hasNext()) {
            return; // If the stream is empty there is no UNA segment
        }
        const Token una = m_scanner.peek();
        if (una.type() == Token::Value && una.value() == QByteArray("UNA")) {
            m_scanner.next() ; // pop peek'ed element
            checkNextEquals(Token(Token::ComponentDataElementSeparator));
            if (m_error) {
                return;
            }
            checkNextEquals(Token(Token::DataElementSeparator));
            if (m_error) {
                return;
            }
            const Token unaContent = m_scanner.peek();
            if (unaContent.value() == QByteArray(". ") || unaContent.value() == QByteArray(", ")) {
                //TODO: properly handle comma (,) as decimal point, #1765
                m_scanner.next();
                if (m_error) {
                    return;
                }
            } else {
                m_error = 1;
                return;
            }
            checkNextEquals(Token(Token::SegmentTerminator));
        }
    }

    void checkNextEquals(const Token& expected) {
        Q_ASSERT(m_scanner.hasNext());
        Token next = m_scanner.next();
        if (next != expected) {
            m_error = 1;
        }
    }

public:
    EdifactTokenReader m_scanner;
    bool m_nextIsValid;
    int m_error;
    Segment m_next;
    bool m_firstSegment;
};

EdifactSegmentReader::EdifactSegmentReader(QIODevice* device)
  : d(new EdifactSegmentReaderPrivate(device))
{
    // Empty
}

EdifactSegmentReader::~EdifactSegmentReader() {
    // Empty
}

int EdifactSegmentReader::errorCode() const {
    return d->m_error;
}

bool EdifactSegmentReader::hasNext() {
    d->ensureNextIsValid();
    return ! d->m_next.isNull();
}

Segment EdifactSegmentReader::next() {
    d->ensureNextIsValid();
    d->m_nextIsValid = false;
    return d->m_next;
}

Segment EdifactSegmentReader::peek() {
    d->ensureNextIsValid();
    return d->m_next;
}
