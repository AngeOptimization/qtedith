#ifndef QTEDITH_VALUE_H
#define QTEDITH_VALUE_H

#include <QSharedDataPointer>
#include "qtedith_export.h"

namespace QtEdith {

class ValueData;

/**
 * The value of a component data element or of a simple data element. Will either be non-present or contain a data value
 * that is a non-empty String.
 *
 * Immutable.
 */
class QTEDITH_EXPORT Value {
    public:
        Value(const Value& other);
        Value& operator=(const Value& other);
        bool operator==(const Value& other) const;
        bool operator!=(const Value& other) const;
        ~Value();
        /**
         * \return true if the value is present
         */
        bool isPresent() const ;
        /**
         * \param defaultString if convertion doesn't succeed
         * \param ok wether or not the convertion succeeds
         * \return value as a bytearray or \param defaultString
         */
        QByteArray asString(QByteArray defaultString, bool* ok = 0) const;
        /**
         * \param ok wether or not the convertion succeeds
         * \return value as a bytearray or a null bytearray
         */
        QByteArray asString(bool* ok = 0) const;

        /**
         * \param defaultDouble default if can't convert
         * \param ok wether or not the convertion succeeds
         * \return the result as double, or defaultDouble if it can't be converted
         */
        double asDouble(double defaultDouble, bool* ok = 0) const;
        /**
         * \param ok wether or not the convertion succeeds
         * \return the value as double, or 0 if it can't be converted
         */
        double asDouble(bool* ok = 0) const;

        /**
         * \param defaultInt default if can't convert
         * \param ok wether or not the convertion succeeds
         * \return the result as int, or defaultInt if it can't be converted
         */
        int asInt(int defaultInt, bool* ok = 0) const;
        /**
         * \param ok wether or not the convertion succeeds
         * \return the value as int, or 0 if it can't be converted
         */
        int asInt(bool* ok = 0) const;

        /**
         * Creates a Value from the bytearray
         */
        static Value valueOf(const QByteArray& array);
    private:
        Value(const QByteArray& array);
        Value();
        QSharedDataPointer<ValueData> d;
};


} // namespace


#endif // QTEDITH_VALUE_H
