#include "predefinedformats.h"

#include <QByteArray>

using namespace QtEdith;

QtEdith::TransitionTable PredefinedFormats::createBaplie95BTransitionTable() {
    TransitionTableBuilder builder;
    // SG0 : UNH-BGM-DTM-(RFF)-(NAD)-SG1-SG2-UNT
    // SG1 : TDT-LOC-DTM(state-10)-RFF-FTX (M1)
    // SG2 : LOC-GID-GDS-FTX-MEA-DIM-TMP-RNG-LOC(state -20)-RFF(state -21)-SG3-SG4 (C9999)
    // SG3 : EQD-EQA-NAD (C9)
    // SG4 : DGS-FTX (C999)
    builder.add("TDT", 0, Transition::to(1));
    builder.add("TDT", -10, Transition::to(1).pop(1));
    builder.add("DTM", 1, Transition::to(-10));
    builder.add("LOC", -10, Transition::to(2).pop(1));
    builder.add("LOC", -20, Transition::to(-20).occurrence(2));
    builder.add("LOC", -21, Transition::to(2).pop(1));
    builder.add("LOC", 3, Transition::to(2).pop(2));
    builder.add("LOC", 4, Transition::to(2).pop(2));
    builder.add("MEA", 2, Transition::to(-20));
    builder.add("RFF", -20, Transition::to(-21));
    builder.add("EQD", -21, Transition::to(3));
    builder.add("EQD", 3, Transition::to(3).pop(1));
    builder.add("DGS", -21, Transition::to(4));
    builder.add("DGS", 3, Transition::to(4).pop(1));
    builder.add("DGS", 4, Transition::to(4).pop(1));
    return builder.build();
}

TransitionTable PredefinedFormats::createCoprar00BTransitionTable() {
    TransitionTableBuilder builder;
    // SG0 : UNH-BGM-DTM-SG2-SG4-SG6-CNT-UNT
    // SG1 : RFF (C1)
    builder.add("RFF", 0, Transition::to(1));
    builder.add("RFF", 1, Transition::to(1).pop(1));
    // SG2 : TDT-RFF-SG3 (M1)
    builder.add("TDT", 0, Transition::to(2));
    builder.add("TDT", 1, Transition::to(2).pop(1));
    builder.add("TDT", 2, Transition::to(2).pop(1));
    builder.add("TDT", 3, Transition::to(2).pop(2));
    // SG3 : LOC-DTM (C9)
    builder.add("LOC", 2, Transition::to(3));
    builder.add("LOC", 3, Transition::to(3).pop(1));
    builder.add("FTX", 3, Transition::to(2).pop(1).createNewGroup(false));
    // SG4 : NAD-SG5 (M9)
    builder.add("NAD", 2, Transition::to(4).pop(1));
    builder.add("NAD", 3, Transition::to(4).pop(2));
    builder.add("NAD", 4, Transition::to(4).pop(1));
    builder.add("NAD", 5, Transition::to(4).pop(2));
    // SG5 : CTA-COM (C9)
    builder.add("CTA", 4, Transition::to(5));
    builder.add("CTA", 5, Transition::to(5).pop(1));
    // SG6 : EQD-RFF-EQN-TMD-DTM-LOC-MEA-DIM-SG7-SEL-FTX-PCD-SG8-EQA-HAN-SG10-NAD (M9999)
    builder.add("EQD", 4, Transition::to(6).pop(1));
    builder.add("EQD", 5, Transition::to(6).pop(2));
    builder.add("EQD", 6, Transition::to(6).pop(1));
    builder.add("EQD", 7, Transition::to(6).pop(2));
    builder.add("EQD", 8, Transition::to(6).pop(2));
    builder.add("EQD", 9, Transition::to(6).pop(3));
    builder.add("EQD", 10, Transition::to(6).pop(2));
    builder.add("EQD", 11, Transition::to(6).pop(3));
    // SG7 : TMP-RNG (C9)
    builder.add("TMP", 6, Transition::to(7));
    builder.add("TMP", 7, Transition::to(7).pop(1));
    // SG6 : EQD-RFF-EQN-TMD-DTM-LOC-MEA-DIM-SG7-SEL-FTX-PCD-SG8-EQA-HAN-SG10-NAD (M9999)
    builder.add("SEL", 7, Transition::to(6).pop(1).createNewGroup(false));
    builder.add("FTX", 7, Transition::to(6).pop(1).createNewGroup(false));
    builder.add("PCD", 7, Transition::to(6).pop(1).createNewGroup(false));
    // SG8 : DGS-FTX-SG9 (C99)
    builder.add("DGS", 6, Transition::to(8));
    builder.add("DGS", 7, Transition::to(8).pop(1));
    builder.add("DGS", 8, Transition::to(8).pop(1));
    builder.add("DGS", 9, Transition::to(8).pop(2));
    // SG9 : CTA-COM (C9)
    builder.add("CTA", 8, Transition::to(9));
    builder.add("CTA", 9, Transition::to(9).pop(1));
    // SG6 : EQD-RFF-EQN-TMD-DTM-LOC-MEA-DIM-SG7-SEL-FTX-PCD-SG8-EQA-HAN-SG10-NAD (M9999)
    builder.add("EQA", 7, Transition::to(6).pop(1).createNewGroup(false));
    builder.add("EQA", 8, Transition::to(6).pop(1).createNewGroup(false));
    builder.add("EQA", 9, Transition::to(6).pop(2).createNewGroup(false));
    builder.add("HAN", 7, Transition::to(6).pop(1).createNewGroup(false));
    builder.add("HAN", 8, Transition::to(6).pop(1).createNewGroup(false));
    builder.add("HAN", 9, Transition::to(6).pop(2).createNewGroup(false));
    // SG10 : TDT-DTM-RFF-SG11 (C1)
    builder.add("TDT", 6, Transition::to(10));
    builder.add("TDT", 7, Transition::to(10).pop(1));
    builder.add("TDT", 8, Transition::to(10).pop(1));
    builder.add("TDT", 9, Transition::to(10).pop(2));
    builder.add("TDT", 10, Transition::to(10).pop(1));
    // SG11 : LOC (C9)
    builder.add("LOC", 10, Transition::to(11));
    builder.add("LOC", 11, Transition::to(11).pop(1));
    // SG6 : EQD-RFF-EQN-TMD-DTM-LOC-MEA-DIM-SG7-SEL-FTX-PCD-SG8-EQA-HAN-SG10-NAD (M9999)
    builder.add("NAD", 7, Transition::to(6).pop(1).createNewGroup(false));
    builder.add("NAD", 8, Transition::to(6).pop(1).createNewGroup(false));
    builder.add("NAD", 9, Transition::to(6).pop(2).createNewGroup(false));
    builder.add("NAD", 10, Transition::to(6).pop(1).createNewGroup(false));
    builder.add("NAD", 11, Transition::to(6).pop(2).createNewGroup(false));
    // SG0 : UNH-BGM-DTM-SG2-SG4-SG6-CNT-UNT
    builder.add("CNT", 6, Transition::to(0).pop(1));
    builder.add("CNT", 7, Transition::to(0).pop(2));
    builder.add("CNT", 8, Transition::to(0).pop(2));
    builder.add("CNT", 9, Transition::to(0).pop(3));
    builder.add("CNT", 10, Transition::to(0).pop(2));
    builder.add("CNT", 11, Transition::to(0).pop(3));
    return builder.build();
}
