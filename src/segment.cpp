#include "segment.h"
#include <QSharedData>
#include <QList>
#include <QByteArray>
#include "value.h"

#include "ediconstants_p.h"

using namespace QtEdith;

class QtEdith::SegmentData : public QSharedData {
    public:
        SegmentData(QByteArray tag) : m_tag(tag) {
            m_composites << QList<QtEdith::Value>();
        }
        QByteArray m_tag;
        QList<QList<QtEdith::Value> > m_composites;
        bool operator==(const QtEdith::SegmentData& other) const {
            return m_tag == other.m_tag && m_composites == other.m_composites;
        }
};

Segment::Segment() : d(0){

}


bool Segment::isNull() const {
    return d==0;
}


Value Segment::data(int compositePosition, int componentComposition) const {
    Q_ASSERT(d);
    return d->m_composites.value(compositePosition-1,QList<QtEdith::Value>()).value(componentComposition-1,QtEdith::Value::valueOf(QByteArray()));
}

QByteArray Segment::tag() const {
    Q_ASSERT(d);
    return d->m_tag;
}

Segment::Segment(const Segment& other) : d(other.d) {

}

Segment& Segment::operator=(const Segment& other) {
    this->d = other.d;
    return *this;
}

bool Segment::operator==(const Segment& other) const {
    if(this->d == other.d) {
        return true;
    }
    if(this->d && other.d) {
        return *(this->d) == *(other.d);
    }
    return false;
}

bool Segment::operator!=(const Segment& other) const {
    return !(*this == other);
}


Segment::Segment(SegmentData* data) : d(data){

}


QByteArray Segment::print() const {
    if(!d) {
        return QByteArray("<nullsegment>");
    }
    QByteArray result;
    result.append(d->m_tag);
    Q_FOREACH (const QList<Value> composite, d->m_composites) {
        result.append(DATA_ELEMENT_SEPARATOR);
        bool firstComponent = true;
        Q_FOREACH (const Value& value, composite) {
            if (!value.isPresent()) {
                continue;
            }
            if (!firstComponent) {
                result.append(COMPONENT_DATA_ELEMENT_SEPARATOR);
            }
            Q_FOREACH(char c, value.asString()) {
                if(QtEdithPrivate::isEdiChar(c)) {
                    result.append(RELEASE_CHARACTER);
                }
                result.append(c);
            }
            firstComponent = false;
        }
    }
    result.append(SEGMENT_TERMINATOR);
    return result;
}

Segment::~Segment() {

}

Segment SegmentBuilder::build() {
    return Segment(d.data());
}

SegmentBuilder& SegmentBuilder::addValue(const QByteArray& array) {
    d->m_composites.last() << (Value::valueOf(array));
    return *this;
}

SegmentBuilder& SegmentBuilder::nextDataElement() {
    d->m_composites << QList<QtEdith::Value>();
    return *this;
}

SegmentBuilder::SegmentBuilder(const QByteArray& tag) : d(new SegmentData(tag)) {

}

SegmentBuilder::~SegmentBuilder() {

}

















