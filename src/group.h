#ifndef QTEDITH_GROUP_H
#define QTEDITH_GROUP_H

#include <QSharedDataPointer>
#include <QScopedPointer>
#include <QList>
#include "segment.h"
#include "qtedith_export.h"

namespace QtEdith {

class GroupData;
class GroupBuilderData;

/**
 * The DOM like representation of an EDIFACT message. Used both for the entire message and for the groups inside it.
 * Will not contain the UNA, UNB or UNZ segments as that is part of the interchange headers that is outside the message.
 *
 * Immutable.
 */
class QTEDITH_EXPORT Group {
    public:
        /**
         * Constructs a null group. Neither segmentList,
         * groupList or debugString is expected to give something useful.
         */
        Group();
        /**
         * copy constructor
         */
        Group(const Group& other);
        ~Group();
        Group& operator=(const Group& other);
        /**
         * returns true if constructed with the default constructor
         */
        bool isNull() const;
        /**
         * \param tag
         * \param occurrence - defaults to 1.
         * \return the list of all segments for the given tag+occurrence combo
         */
        QList<QtEdith::Segment> segmentList(const QByteArray& tag, int occurrence = 1) const;
        /**
         * \param groupNumber
         * \return the list of all groups for the given group number
         */
        QList<Group> groupList(int groupNumber) const;
        /**
         * \return human readable representation of this group
         */
        QString debugString();
    private:
        /**
         * constructor used by the builder
         */
        Group(GroupBuilderData* data);
        QSharedDataPointer<GroupData> d;
        friend class GroupBuilder;
};

/**
 * Builder for \link Group
 */
class QTEDITH_EXPORT GroupBuilder {
    public:
        /**
         * Creates a group builder
         */
        GroupBuilder();
        void add(const QtEdith::Segment& segment, int occurrence = 1);
        /**
         * Adds a group builder for a given group and group number
         * Takes ownership over the group builder
         * \param groupNumber number to add
         * \param groupbuilder builder for the added group
         */
        void add(int groupNumber, GroupBuilder* groupbuilder);
        /**
         * \return newly built Group.
         */
        Group build();
        ~GroupBuilder();
    private:
        Q_DISABLE_COPY(GroupBuilder);
        QScopedPointer<GroupBuilderData> d;
        friend class Group;
};
}

#endif // QTEDITH_GROUP_H
