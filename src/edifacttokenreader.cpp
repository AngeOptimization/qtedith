#include "edifacttokenreader.h"

#include "ediconstants_p.h"

#include <QIODevice>

using namespace QtEdith;

class QtEdith::EdifactTokenReaderPrivate {
public:
    EdifactTokenReaderPrivate(QIODevice* device) : m_device(device), m_nextIsValid(false), m_error(0) {
        Q_ASSERT(device->isOpen());
        Q_ASSERT(device->isReadable());
    }
    QIODevice* m_device;
    Token m_next;
    bool m_nextIsValid;
    int m_error;
    void ensureNextIsValid() {
        if (m_nextIsValid) {
            return;
        }
        m_next = readNextToken();
        m_nextIsValid = true;
    }
    char readNonLineTermination() {
        char value;
        bool success;
        do {
            success = m_device->getChar(&value);
        } while (success && (value == '\n' || value == '\r'));
        if (!success) {
            // expected at EOF
            return -1;
        }
        return value;
    }
    Token readDataElementValue(char c) {
        char nextByte = c;
        QByteArray bytes;
        while (true) {
            if (nextByte == RELEASE_CHARACTER) {
                nextByte = readNonLineTermination();
                if (nextByte == -1) {
                    m_error = EdifactTokenReader::ReleaseCharAtEOF;
                    return Token();
                }
            } else {
                if (QtEdithPrivate::isEdiChar(nextByte)) {
                    m_device->ungetChar(nextByte);
                    return Token(bytes);
                }
            }
            bytes.append(nextByte);
            nextByte = readNonLineTermination();
            if (nextByte == -1) {
                m_error = EdifactTokenReader::NoTerminatorAtEOF;
                return Token(bytes);
            }

        }
    }
    Token readNextToken() {
        char c = readNonLineTermination();
        switch (c) {
            case -1: {
                return Token();
            }
            case COMPONENT_DATA_ELEMENT_SEPARATOR: {
                return Token(Token::ComponentDataElementSeparator);
            }
            case DATA_ELEMENT_SEPARATOR: {
                return Token(Token::DataElementSeparator);
            }
            case SEGMENT_TERMINATOR: {
                return Token(Token::SegmentTerminator);
            }
            default: {
                return readDataElementValue(c);
            }
        }
        Q_ASSERT(false);
        return Token();
    }
};

EdifactTokenReader::EdifactTokenReader(QIODevice* device)
  : d(new EdifactTokenReaderPrivate(device))
{
    // Empty
}

EdifactTokenReader::~EdifactTokenReader() {
    // Empty
}

Token EdifactTokenReader::next() {
    d->ensureNextIsValid();
    d->m_nextIsValid = false;
    return d->m_next;
}

Token EdifactTokenReader::peek() {
    d->ensureNextIsValid();
    return d->m_next;
}

bool EdifactTokenReader::hasNext() {
    d->ensureNextIsValid();
    return !d->m_next.isNull();
}

int EdifactTokenReader::errorCode() const {
    return d->m_error;
}
