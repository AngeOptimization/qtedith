#include "edifactsegmentreader.h"

#include <group.h>
#include <predefinedformats.h>
#include <segment.h>
#include <segmentgrouper.h>

#include <QDebug>
#include <QFile>
#include <QTextStream>

using namespace QtEdith;

int main () {
    QFile in;
    in.open(stdin, QIODevice::ReadOnly);
    QTextStream out(stdout);

    EdifactSegmentReader reader(&in);

    // Check UNB
    if (!reader.hasNext()) {
        qDebug() << "ERROR:" << __LINE__ << "UNB segment missing";
        return 1;
    }
    Segment unb = reader.next();
    if (unb.tag() != QByteArrayLiteral("UNB"))  {
        qDebug() << "ERROR:" << __LINE__ << QString("Wrong segment where UNB expected: %1").arg(QString(unb.print()));
        return 2;
    }

    // Check UNH
    if (!reader.hasNext()) {
        qDebug() << "ERROR:" << __LINE__ << "UNH segment missing";
        return 3;
    }
    Segment unh = reader.peek();
    if (unh.tag() != QByteArrayLiteral("UNH"))  {
        qDebug() << "ERROR:" << __LINE__ << QString("Wrong segment where UNH expected: %1").arg(QString(unh.print()));
        return 4;
    }

    // Parse the message (UNH..UNT)
    SegmentGrouper grouper(PredefinedFormats::createBaplie95BTransitionTable());
    const Group& group0 = grouper.group(&reader);
    if (grouper.errorCode() != 0) {
        qDebug() << "ERROR:" << __LINE__ << QStringLiteral("Bad structure in BAPLIE file, error code %1").arg(grouper.errorCode());
        return 5;
    }

    // Check UNZ
    if (!reader.hasNext()) {
        qDebug() << "ERROR:" << __LINE__ << "UNZ segment missing";
        return 6;
    }
    Segment unz = reader.next();
    if (unz.tag() != QByteArrayLiteral("UNZ"))  {
        qDebug() << "ERROR:" << __LINE__ << QString("Wrong segment where UNZ expected: %1").arg(QString(unz.print()));
        return 7;
    }

    // Check end of file
    if (reader.hasNext()) {
        qDebug() << "ERROR:" << __LINE__ << "Data after UNZ segment";
        return 8;
    }

    if (reader.errorCode()) {
        qDebug() << "ERROR:" << __LINE__ << QString("Error from EdifactSegmentReader: %1").arg(reader.errorCode());
        return 9;
    }

    qDebug() << "EXIT:" << __LINE__;
}
