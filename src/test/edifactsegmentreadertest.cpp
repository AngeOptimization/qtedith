#include <QtTest>
#include <edifactsegmentreader.h>
#include "testprettyprinters.h"

using namespace QtEdith;

class EdifactSegmentReaderTest : public QObject {
    Q_OBJECT
    private Q_SLOTS:
        void testBadUna();
        void testGoodUna();
        void testSegmentsWithNewline();
        void testSegmentsWithNlCr();
        void testSegmentsConcatenated();
        void testNonTerminatedSegment();
        void testEmpty();
};

void EdifactSegmentReaderTest::testBadUna() {
    QByteArray array("UNA:+k? 'EQD+CN+ZZZU0000004+42G1+++5'");
    QBuffer buffer(&array);
    buffer.open(QIODevice::ReadOnly);

    QtEdith::EdifactSegmentReader reader(&buffer);
    QVERIFY(!reader.hasNext());
    QCOMPARE(reader.errorCode(),1);

}

void EdifactSegmentReaderTest::testGoodUna() {
    QByteArray array("UNA:+.? 'EQD+CN+ZZZU0000004+42G1+++5'");
    QBuffer buffer(&array);
    buffer.open(QIODevice::ReadOnly);

    QtEdith::EdifactSegmentReader reader(&buffer);
    reader.hasNext();
    QVERIFY(reader.hasNext());
    QCOMPARE(reader.next().tag(),QByteArray("EQD"));
    QCOMPARE(reader.errorCode(),0);
}

void EdifactSegmentReaderTest::testSegmentsConcatenated() {
    QByteArray array("HES+TE:111'EQD+CN+ZZZU0000004+42G1+++5'");
    QBuffer buffer(&array);
    buffer.open(QIODevice::ReadOnly);
    QtEdith::EdifactSegmentReader reader(&buffer);
    reader.hasNext();
    QVERIFY(reader.hasNext());
    Segment segment = reader.next();
    QVERIFY(!segment.isNull());
    QCOMPARE(segment.tag(),QByteArray("HES"));
    QCOMPARE(segment.data(1,1), Value::valueOf("TE"));
    QCOMPARE(segment.data(1,2), Value::valueOf("111"));
    QVERIFY(reader.hasNext());
    segment = reader.next();
    QVERIFY(!segment.isNull());
    QCOMPARE(segment.tag(),QByteArray("EQD"));
    QCOMPARE(segment.data(1,1), Value::valueOf("CN"));
    QCOMPARE(segment.data(2,1), Value::valueOf("ZZZU0000004"));
    QCOMPARE(segment.data(3,1), Value::valueOf("42G1"));
    QCOMPARE(segment.data(4,1), Value::valueOf(QByteArray()));
    QCOMPARE(segment.data(5,1), Value::valueOf(QByteArray()));
    QCOMPARE(segment.data(6,1), Value::valueOf("5"));
    QVERIFY(!reader.hasNext());
}

void EdifactSegmentReaderTest::testSegmentsWithNewline() {
    QByteArray array("HES+TE:111'\nEQD+CN+ZZZU0000004+42G1+++5'");
    QBuffer buffer(&array);
    buffer.open(QIODevice::ReadOnly);
    QtEdith::EdifactSegmentReader reader(&buffer);
    reader.hasNext();
    QVERIFY(reader.hasNext());
    Segment segment = reader.next();
    QVERIFY(!segment.isNull());
    QCOMPARE(segment.tag(),QByteArray("HES"));
    QCOMPARE(segment.data(1,1), Value::valueOf("TE"));
    QCOMPARE(segment.data(1,2), Value::valueOf("111"));
    QVERIFY(reader.hasNext());
    segment = reader.next();
    QVERIFY(!segment.isNull());
    QCOMPARE(segment.tag(),QByteArray("EQD"));
    QCOMPARE(segment.data(1,1), Value::valueOf("CN"));
    QCOMPARE(segment.data(2,1), Value::valueOf("ZZZU0000004"));
    QCOMPARE(segment.data(3,1), Value::valueOf("42G1"));
    QCOMPARE(segment.data(4,1), Value::valueOf(QByteArray()));
    QCOMPARE(segment.data(5,1), Value::valueOf(QByteArray()));
    QCOMPARE(segment.data(6,1), Value::valueOf("5"));
    QVERIFY(!reader.hasNext());

}

void EdifactSegmentReaderTest::testSegmentsWithNlCr() {
    QByteArray array("HES+TE:111'\n\rEQD+CN+ZZZU0000004+42G1+++5'");
    QBuffer buffer(&array);
    buffer.open(QIODevice::ReadOnly);
    QtEdith::EdifactSegmentReader reader(&buffer);
    reader.hasNext();
    QVERIFY(reader.hasNext());
    Segment segment = reader.next();
    QVERIFY(!segment.isNull());
    QCOMPARE(segment.tag(),QByteArray("HES"));
    QCOMPARE(segment.data(1,1), Value::valueOf("TE"));
    QCOMPARE(segment.data(1,2), Value::valueOf("111"));
    QVERIFY(reader.hasNext());
    segment = reader.next();
    QVERIFY(!segment.isNull());
    QCOMPARE(segment.tag(),QByteArray("EQD"));
    QCOMPARE(segment.data(1,1), Value::valueOf("CN"));
    QCOMPARE(segment.data(2,1), Value::valueOf("ZZZU0000004"));
    QCOMPARE(segment.data(3,1), Value::valueOf("42G1"));
    QCOMPARE(segment.data(4,1), Value::valueOf(QByteArray()));
    QCOMPARE(segment.data(5,1), Value::valueOf(QByteArray()));
    QCOMPARE(segment.data(6,1), Value::valueOf("5"));
    QVERIFY(!reader.hasNext());
}

void EdifactSegmentReaderTest::testNonTerminatedSegment() {
    QByteArray array("HES+TE:111'\nEQD+CN+ZZZU0000004+42G1+++5");
    QBuffer buffer(&array);
    buffer.open(QIODevice::ReadOnly);
    QtEdith::EdifactSegmentReader reader(&buffer);
    reader.hasNext();
    QVERIFY(reader.hasNext());
    Segment segment = reader.next();
    QVERIFY(!segment.isNull());
    QCOMPARE(segment.tag(),QByteArray("HES"));
    QCOMPARE(segment.data(1,1), Value::valueOf("TE"));
    QCOMPARE(segment.data(1,2), Value::valueOf("111"));
    QVERIFY(!reader.hasNext());
    segment = reader.next();
    QVERIFY(segment.isNull());
}

void EdifactSegmentReaderTest::testEmpty() {
    QBuffer buffer;
    buffer.open(QIODevice::ReadOnly);
    EdifactSegmentReader reader(&buffer);
    QVERIFY(!reader.hasNext());
    QCOMPARE(reader.errorCode(), 0); // The empty stream has no segment error, the error is on a higher level
}

QTEST_GUILESS_MAIN(EdifactSegmentReaderTest);

#include "edifactsegmentreadertest.moc"
