#include <QTest>
#include <QFile>
#include "predefinedformats.h"
#include "segmentgrouper.h"
#include "edifactsegmentreader.h"

using namespace QtEdith;

/*
 * A test to ensure SMDG22 (in particular VGM - Verified Gross Mass) is properly
 * read.
 */

class Baplie95BReaderTest : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void testBaplie95BGrouper01();
};

void Baplie95BReaderTest::testBaplie95BGrouper01() {
    QString testfile = QFINDTESTDATA("smdg22data/FG30851WLKCMBange.edi");
    QVERIFY(QFile::exists(testfile));

    QFile f(testfile);
    QVERIFY(f.open(QIODevice::ReadOnly));

    EdifactSegmentReader reader(&f);
    Segment s = reader.next();
    QCOMPARE(s.tag(),QByteArray("UNB"));

    SegmentGrouper grouper(PredefinedFormats::createBaplie95BTransitionTable());

    QList<Group> messages;

    while (reader.peek().tag() == QByteArray("UNH")) {

        Segment unh = reader.peek();
        QCOMPARE(unh.tag(),QByteArray("UNH"));
        QCOMPARE(unh.data(2, 1).asString(), QByteArray("BAPLIE"));
        QCOMPARE(unh.data(2, 2).asString(), QByteArray("D"));
        QCOMPARE(unh.data(2, 3).asString(), QByteArray("95B"));
        QCOMPARE(unh.data(2, 4).asString(), QByteArray("UN") );
        QCOMPARE(unh.data(2, 5).asString(), QByteArray("SMDG22") );

        Group coprar = grouper.group(&reader);
        QVERIFY(!coprar.isNull());
        messages << coprar;

        QCOMPARE(grouper.errorCode(), 0);
    }

    Segment unz = reader.next();
    QCOMPARE(unz.tag(), QByteArray("UNZ"));
    QVERIFY(!reader.hasNext());

    QCOMPARE(messages.size(), 1);

    Group message = messages.first();

    QVERIFY(!message.segmentList("BGM").isEmpty());
    QVERIFY(!message.segmentList("DTM").isEmpty());
    QVERIFY(message.segmentList("RFF").isEmpty());
    QVERIFY(message.segmentList("NAD").isEmpty());

    QCOMPARE(message.groupList(1).size(),1);

    Group group1 = message.groupList(1).first();

    QVERIFY(!group1.segmentList("TDT").isEmpty());
    QCOMPARE(group1.segmentList("LOC").size(), 2);
    QCOMPARE(group1.segmentList("DTM").size(), 2);
    QCOMPARE(group1.segmentList("RFF").size(), 0);
    QCOMPARE(group1.segmentList("FTX").size(), 0);

    QCOMPARE(message.groupList(2).size(), 1264);

    QList<Group> group2 = message.groupList(2);

    {
    /*
     * 1st container
     *
     * LOC+147+0030782::5'
     * MEA+VGM++KGM:7830'
     * LOC+9+CNNSA:139:6'
     * LOC+11+NGTIN:139:6'
     * LOC+83+NGTCI:139:6'
     * RFF+BM:1'
     * EQD+CN+TRHU3223413+2200+++5'
     * NAD+CA+ZIM:172:20'
     */
    QCOMPARE(group2.at(0).segmentList("LOC").size(), 1);
    QCOMPARE(group2.at(0).segmentList("MEA").size(), 1);
    QCOMPARE(group2.at(0).segmentList("DIM").size(), 0);
    QCOMPARE(group2.at(0).segmentList("TMP").size(), 0);
    QCOMPARE(group2.at(0).segmentList("RNG").size(), 0);
    QCOMPARE(group2.at(0).segmentList("LOC",2).size(), 3);
    QCOMPARE(group2.at(0).groupList(3).size(), 1);

    Group group3 = group2.at(0).groupList(3).first();

    QCOMPARE(group3.segmentList("EQD").size(), 1);
    QCOMPARE(group3.segmentList("EQA").size(), 0);
    QCOMPARE(group3.segmentList("NAD").size(), 1);

    QCOMPARE(group2.at(0).groupList(4).size(), 0);
    }
}

QTEST_GUILESS_MAIN(Baplie95BReaderTest);
#include "baplie22readertest.moc"
