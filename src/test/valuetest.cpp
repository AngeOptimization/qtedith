#include <QtTest>
#include "value.h"

using QtEdith::Value;

class ValueTest : public QObject {
    Q_OBJECT
    private Q_SLOTS:
        void testValue01();
        void testValue02();
        void testValue03();
        void testValue04();
};

void ValueTest::testValue01() {
    Value valueNumber = Value::valueOf("123456789");
    QVERIFY(valueNumber.isPresent());
    bool ok;
    QCOMPARE(valueNumber.asString(&ok), QByteArray("123456789"));
    QVERIFY(ok);
    QCOMPARE(valueNumber.asDouble(0, &ok), double(123456789));
    QVERIFY(ok);
}

void ValueTest::testValue02() {
    Value valueString = Value::valueOf("IFTSAI");
    QVERIFY(valueString.isPresent());
    bool ok;
    QCOMPARE(valueString.asString("", &ok), QByteArray("IFTSAI"));
    QVERIFY(ok);
    QCOMPARE(valueString.asDouble(&ok),double(0));
    QVERIFY(!ok);
}

void ValueTest::testValue03() {
    Value valueEmptyString = Value::valueOf("");
    QVERIFY(!valueEmptyString.isPresent());
    bool ok;
    QVERIFY(valueEmptyString.asString(QByteArray(),&ok).isNull());
    QVERIFY(!ok);
    // empty string is treated as non-present, does not throw
    QCOMPARE(valueEmptyString.asDouble(double(0)), double(0));
}

void ValueTest::testValue04() {
    Value valueNull = Value::valueOf(QByteArray());
    QVERIFY(!valueNull.isPresent());
    QVERIFY(valueNull.asString(QByteArray()).isNull());
    QCOMPARE(valueNull.asDouble(double(0)), double(0));
}



QTEST_GUILESS_MAIN(ValueTest)

#include "valuetest.moc"
