#include <QTest>
#include <QFile>
#include "predefinedformats.h"
#include "segmentgrouper.h"
#include "edifactsegmentreader.h"

using namespace QtEdith;

class Coprar00BReaderTest : public QObject {
    Q_OBJECT
    private Q_SLOTS:
        void testCoprar00BGrouper01();
        void testCoprar00BGrouper02();
        void testCoprar00BGrouper03();
        void testCoprar00BGrouper04();
};

void Coprar00BReaderTest::testCoprar00BGrouper01() {
    QString testfile = QFINDTESTDATA("coprar00Bdata/BK2COPRARYL74_1420_W_ESALG_X_5110156.edi");
    QVERIFY(QFile::exists(testfile));

    QFile f(testfile);
    QVERIFY(f.open(QIODevice::ReadOnly));

    EdifactSegmentReader reader(&f);
    Segment s = reader.next();
    QCOMPARE(s.tag(),QByteArray("UNB"));

    SegmentGrouper grouper(PredefinedFormats::createCoprar00BTransitionTable());

    QList<Group> messages;

    while (reader.peek().tag() == QByteArray("UNH")) {

        Segment unh = reader.peek();
        QCOMPARE(unh.tag(),QByteArray("UNH"));
        QCOMPARE(unh.data(2, 1).asString(), QByteArray("COPRAR"));
        QCOMPARE(unh.data(2, 2).asString(), QByteArray("D"));
        QCOMPARE(unh.data(2, 3).asString(), QByteArray("00B"));
        QCOMPARE(unh.data(2, 4).asString(), QByteArray("UN") );

        Group coprar = grouper.group(&reader);
        QVERIFY(!coprar.isNull());
        messages << coprar;

        QCOMPARE(grouper.errorCode(),0);
    }

    Segment unz = reader.next();
    QCOMPARE(unz.tag(), QByteArray("UNZ"));
    QVERIFY(!reader.hasNext());

    QCOMPARE(messages.size(),1);

    Group message = messages.first();


    QCOMPARE(message.groupList(1).size(),1);
    Group group1 = message.groupList(1).first();
    QCOMPARE(group1.segmentList("RFF").size(),1);
    QVERIFY(!group1.segmentList("RFF").first().isNull());

    QCOMPARE(message.groupList(2).size(),1);

    Group group2 = message.groupList(2).first();

    QCOMPARE(group2.segmentList("TDT").size(),1);
    QVERIFY(!group2.segmentList("TDT").first().isNull());
    QCOMPARE(group2.segmentList("RFF").size(),1);
    QVERIFY(!group2.segmentList("RFF").first().isNull());

    QCOMPARE(group2.groupList(3).size(),1);
    QVERIFY(!group2.groupList(3).first().segmentList("LOC").isEmpty());
    QVERIFY(!group2.groupList(3).first().segmentList("LOC").first().isNull());

    QCOMPARE(message.groupList(4).size(),2);
    QList<Group> group4 = message.groupList(4);
    QVERIFY(!group4.at(0).segmentList("NAD").isEmpty());
    QVERIFY(!group4.at(1).segmentList("NAD").isEmpty());

    QVERIFY(message.groupList(4).at(0).groupList(5).isEmpty());
    QVERIFY(message.groupList(4).at(1).groupList(5).isEmpty());

    QCOMPARE(message.groupList(6).size(),2);

    QList<Group> group6 = message.groupList(6);

    QVERIFY(!group6.at(0).segmentList("EQD").isEmpty());
    QCOMPARE(group6.at(0).segmentList("EQD").first().tag(), QByteArray("EQD"));
    QCOMPARE(group6.at(0).segmentList("RFF").size(),2);
    QCOMPARE(group6.at(0).segmentList("EQN").size(),0);
    QCOMPARE(group6.at(0).segmentList("TMD").size(),0);
    QCOMPARE(group6.at(0).segmentList("DTM").size(),0);
    QCOMPARE(group6.at(0).segmentList("LOC").size(),3);
    QCOMPARE(group6.at(0).segmentList("MEA").size(),1);
    QCOMPARE(group6.at(0).segmentList("DIM").size(),0);
    QCOMPARE(group6.at(0).groupList(7).size(),0);
    QCOMPARE(group6.at(0).segmentList("SEL").size(),0);
    QCOMPARE(group6.at(0).segmentList("FTX").size(),0);
    QCOMPARE(group6.at(0).groupList(8).size(),0);
    QCOMPARE(group6.at(0).segmentList("EQA").size(),0);
    QCOMPARE(group6.at(0).segmentList("HAN").size(),0);
    QCOMPARE(group6.at(0).groupList(10).size(),0);
    QCOMPARE(group6.at(0).segmentList("NAD").size(),2);

    QVERIFY(!group6.at(1).segmentList("EQD").isEmpty());
    QCOMPARE(group6.at(1).segmentList("EQD").first().tag(), QByteArray("EQD"));
    QCOMPARE(group6.at(1).segmentList("RFF").size(),2);
    QCOMPARE(group6.at(1).segmentList("EQN").size(),0);
    QCOMPARE(group6.at(1).segmentList("TMD").size(),0);
    QCOMPARE(group6.at(1).segmentList("DTM").size(),0);
    QCOMPARE(group6.at(1).segmentList("LOC").size(),3);
    QCOMPARE(group6.at(1).segmentList("MEA").size(),1);
    QCOMPARE(group6.at(1).segmentList("DIM").size(),0);
    QCOMPARE(group6.at(1).groupList(7).size(),0);
    QCOMPARE(group6.at(1).segmentList("SEL").size(),0);
    QCOMPARE(group6.at(1).segmentList("FTX").size(),0);
    QCOMPARE(group6.at(1).groupList(8).size(),0);
    QCOMPARE(group6.at(1).segmentList("EQA").size(),0);
    QCOMPARE(group6.at(1).segmentList("HAN").size(),0);
    QCOMPARE(group6.at(1).groupList(10).size(),0);
    QCOMPARE(group6.at(1).segmentList("NAD").size(),2);
}

void Coprar00BReaderTest::testCoprar00BGrouper02() {
    QString testfile = QFINDTESTDATA("coprar00Bdata/COS_COPRAR_CARGA_AL_RAIN_004W.edi");
    QVERIFY(QFile::exists(testfile));

    QFile f(testfile);
    QVERIFY(f.open(QIODevice::ReadOnly));

    EdifactSegmentReader reader(&f);
    Segment s = reader.next();
    QCOMPARE(s.tag(),QByteArray("UNB"));

    SegmentGrouper grouper(PredefinedFormats::createCoprar00BTransitionTable());

    QList<Group> messages;

    while (reader.peek().tag() == QByteArray("UNH")) {

        Segment unh = reader.peek();
        QCOMPARE(unh.tag(),QByteArray("UNH"));
        QCOMPARE(unh.data(2, 1).asString(), QByteArray("COPRAR"));
        QCOMPARE(unh.data(2, 2).asString(), QByteArray("D"));
        QCOMPARE(unh.data(2, 3).asString(), QByteArray("00B"));
        QCOMPARE(unh.data(2, 4).asString(), QByteArray("UN") );

        Group coprar = grouper.group(&reader);
        QVERIFY(!coprar.isNull());
        messages << coprar;

        QCOMPARE(grouper.errorCode(),0);
    }

    Segment unz = reader.next();
    QCOMPARE(unz.tag(), QByteArray("UNZ"));
    QVERIFY(!reader.hasNext());

    QCOMPARE(messages.size(),1);

    Group message = messages.first();

    QCOMPARE(message.groupList(1).size(),1);
    Group group1 = message.groupList(1).first();

    QVERIFY(!group1.segmentList("RFF").isEmpty());

    QCOMPARE(message.groupList(2).size(),1);

    Group group2 = message.groupList(2).first();
    QVERIFY(!group2.segmentList("TDT").isEmpty());
    QVERIFY(!group2.segmentList("RFF").isEmpty());

    QCOMPARE(group2.groupList(3).size(),1);

    Group group3 = group2.groupList(3).first();

    QVERIFY(!group3.segmentList("LOC").isEmpty());

    QCOMPARE(message.groupList(4).size(), 2);

    QList<Group> group4 = message.groupList(4);

    QVERIFY(!group4.at(0).segmentList("NAD").isEmpty());
    QVERIFY(!group4.at(1).segmentList("NAD").isEmpty());

    QVERIFY(group4.at(0).groupList(5).isEmpty());
    QVERIFY(group4.at(0).groupList(5).isEmpty());

    QCOMPARE(message.groupList(6).size(), 3);

    QList<Group> group6 = message.groupList(6);

    QVERIFY(!group6.at(0).segmentList("EQD").isEmpty());
    QCOMPARE(group6.at(0).segmentList("EQD").first().tag(), QByteArray("EQD"));
    QCOMPARE(group6.at(0).segmentList("RFF").size(),1);
    QCOMPARE(group6.at(0).segmentList("EQN").size(),0);
    QCOMPARE(group6.at(0).segmentList("TMD").size(),0);
    QCOMPARE(group6.at(0).segmentList("DTM").size(),0);
    QCOMPARE(group6.at(0).segmentList("LOC").size(),2);
    QCOMPARE(group6.at(0).segmentList("MEA").size(),1);
    QCOMPARE(group6.at(0).segmentList("DIM").size(),0);
    QCOMPARE(group6.at(0).groupList(7).size(),0);
    QCOMPARE(group6.at(0).segmentList("SEL").size(),1);
    QCOMPARE(group6.at(0).segmentList("FTX").size(),0);
    QCOMPARE(group6.at(0).groupList(8).size(),0);
    QCOMPARE(group6.at(0).segmentList("EQA").size(),0);
    QCOMPARE(group6.at(0).segmentList("HAN").size(),0);
    QCOMPARE(group6.at(0).groupList(10).size(),0);
    QCOMPARE(group6.at(0).segmentList("NAD").size(),1);

    QVERIFY(!group6.at(1).segmentList("EQD").isEmpty());
    QCOMPARE(group6.at(1).segmentList("EQD").first().tag(), QByteArray("EQD"));
    QCOMPARE(group6.at(1).segmentList("RFF").size(),1);
    QCOMPARE(group6.at(1).segmentList("EQN").size(),0);
    QCOMPARE(group6.at(1).segmentList("TMD").size(),0);
    QCOMPARE(group6.at(1).segmentList("DTM").size(),0);
    QCOMPARE(group6.at(1).segmentList("LOC").size(),2);
    QCOMPARE(group6.at(1).segmentList("MEA").size(),1);
    QCOMPARE(group6.at(1).segmentList("DIM").size(),0);
    QCOMPARE(group6.at(1).groupList(7).size(),0);
    QCOMPARE(group6.at(1).segmentList("SEL").size(),1);
    QCOMPARE(group6.at(1).segmentList("FTX").size(),0);
    QCOMPARE(group6.at(1).groupList(8).size(),0);
    QCOMPARE(group6.at(1).segmentList("EQA").size(),0);
    QCOMPARE(group6.at(1).segmentList("HAN").size(),0);
    QCOMPARE(group6.at(1).groupList(10).size(),0);
    QCOMPARE(group6.at(1).segmentList("NAD").size(),1);

    QVERIFY(!group6.at(2).segmentList("EQD").isEmpty());
    QCOMPARE(group6.at(2).segmentList("EQD").first().tag(), QByteArray("EQD"));
    QCOMPARE(group6.at(2).segmentList("RFF").size(),1);
    QCOMPARE(group6.at(2).segmentList("EQN").size(),0);
    QCOMPARE(group6.at(2).segmentList("TMD").size(),0);
    QCOMPARE(group6.at(2).segmentList("DTM").size(),0);
    QCOMPARE(group6.at(2).segmentList("LOC").size(),2);
    QCOMPARE(group6.at(2).segmentList("MEA").size(),1);
    QCOMPARE(group6.at(2).segmentList("DIM").size(),0);
    QCOMPARE(group6.at(2).groupList(7).size(),0);
    QCOMPARE(group6.at(2).segmentList("SEL").size(),1);
    QCOMPARE(group6.at(2).segmentList("FTX").size(),0);
    QCOMPARE(group6.at(2).groupList(8).size(),0);
    QCOMPARE(group6.at(2).segmentList("EQA").size(),0);
    QCOMPARE(group6.at(2).segmentList("HAN").size(),0);
    QCOMPARE(group6.at(2).groupList(10).size(),0);
    QCOMPARE(group6.at(2).segmentList("NAD").size(),1);
}

void Coprar00BReaderTest::testCoprar00BGrouper03() {
    QString testfile = QFINDTESTDATA("coprar00Bdata/EDIGTR868271.edi");
    QVERIFY(QFile::exists(testfile));

    QFile f(testfile);
    QVERIFY(f.open(QIODevice::ReadOnly));

    EdifactSegmentReader reader(&f);
    Segment s = reader.next();
    QCOMPARE(s.tag(),QByteArray("UNB"));

    SegmentGrouper grouper(PredefinedFormats::createCoprar00BTransitionTable());

    QList<Group> messages;

    while (reader.peek().tag() == QByteArray("UNH")) {

        Segment unh = reader.peek();
        QCOMPARE(unh.tag(),QByteArray("UNH"));
        QCOMPARE(unh.data(2, 1).asString(), QByteArray("COPRAR"));
        QCOMPARE(unh.data(2, 2).asString(), QByteArray("D"));
        QCOMPARE(unh.data(2, 3).asString(), QByteArray("00B"));
        QCOMPARE(unh.data(2, 4).asString(), QByteArray("UN") );

        Group coprar = grouper.group(&reader);
        QVERIFY(!coprar.isNull());
        messages << coprar;

        QCOMPARE(grouper.errorCode(),0);
    }

    Segment unz = reader.next();
    QCOMPARE(unz.tag(), QByteArray("UNZ"));
    QVERIFY(!reader.hasNext());

    QCOMPARE(messages.size(),1);

    Group message = messages.first();

    QCOMPARE(message.groupList(1).size(),1);
    Group group1 = message.groupList(1).first();

    QVERIFY(!group1.segmentList("RFF").isEmpty());

    QCOMPARE(message.groupList(2).size(),1);

    Group group2 = message.groupList(2).first();
    QVERIFY(!group2.segmentList("TDT").isEmpty());
    QVERIFY(!group2.segmentList("RFF").isEmpty());

    QCOMPARE(group2.groupList(3).size(),1);

    Group group3 = group2.groupList(3).first();

    QVERIFY(!group3.segmentList("LOC").isEmpty());

    QCOMPARE(message.groupList(4).size(), 2);

    QList<Group> group4 = message.groupList(4);

    QVERIFY(!group4.at(0).segmentList("NAD").isEmpty());
    QVERIFY(!group4.at(1).segmentList("NAD").isEmpty());

    QVERIFY(group4.at(0).groupList(5).isEmpty());
    QVERIFY(group4.at(1).groupList(5).isEmpty());

    QCOMPARE(message.groupList(6).size(), 48);

    QList<Group> group6 = message.groupList(6);

    QVERIFY(!group6.at(0).segmentList("EQD").isEmpty());
    QCOMPARE(group6.at(0).segmentList("EQD").first().tag(), QByteArray("EQD"));
    QCOMPARE(group6.at(0).segmentList("RFF").size(),2);
    QCOMPARE(group6.at(0).segmentList("EQN").size(),0);
    QCOMPARE(group6.at(0).segmentList("TMD").size(),0);
    QCOMPARE(group6.at(0).segmentList("DTM").size(),0);
    QCOMPARE(group6.at(0).segmentList("LOC").size(),3);
    QCOMPARE(group6.at(0).segmentList("MEA").size(),1);
    QCOMPARE(group6.at(0).segmentList("DIM").size(),0);
    QCOMPARE(group6.at(0).groupList(7).size(),0);
    QCOMPARE(group6.at(0).segmentList("SEL").size(),1);
    QCOMPARE(group6.at(0).segmentList("FTX").size(),0);
    QCOMPARE(group6.at(0).groupList(8).size(),0);
    QCOMPARE(group6.at(0).segmentList("EQA").size(),0);
    QCOMPARE(group6.at(0).segmentList("HAN").size(),0);
    QCOMPARE(group6.at(0).groupList(10).size(),0);
    QCOMPARE(group6.at(0).segmentList("NAD").size(),0);

    QVERIFY(!group6.at(47).segmentList("EQD").isEmpty());
    QCOMPARE(group6.at(47).segmentList("EQD").first().tag(), QByteArray("EQD"));
    QCOMPARE(group6.at(47).segmentList("RFF").size(),2);
    QCOMPARE(group6.at(47).segmentList("EQN").size(),0);
    QCOMPARE(group6.at(47).segmentList("TMD").size(),0);
    QCOMPARE(group6.at(47).segmentList("DTM").size(),0);
    QCOMPARE(group6.at(47).segmentList("LOC").size(),3);
    QCOMPARE(group6.at(47).segmentList("MEA").size(),1);
    QCOMPARE(group6.at(47).segmentList("DIM").size(),0);
    QCOMPARE(group6.at(47).groupList(7).size(),0);
    QCOMPARE(group6.at(47).segmentList("SEL").size(),1);
    QCOMPARE(group6.at(47).segmentList("FTX").size(),0);
    QCOMPARE(group6.at(47).groupList(8).size(),0);
    QCOMPARE(group6.at(47).segmentList("EQA").size(),0);
    QCOMPARE(group6.at(47).segmentList("HAN").size(),0);
    QCOMPARE(group6.at(47).groupList(10).size(),0);
    QCOMPARE(group6.at(47).segmentList("NAD").size(),0);
}

void Coprar00BReaderTest::testCoprar00BGrouper04() {
    QString testfile = QFINDTESTDATA("coprar00Bdata/00BCMA_TRIZT_COPRAR.txt");
    QVERIFY(QFile::exists(testfile));

    QFile f(testfile);
    QVERIFY(f.open(QIODevice::ReadOnly));

    EdifactSegmentReader reader(&f);
    Segment s = reader.next();
    QCOMPARE(s.tag(),QByteArray("UNB"));

    SegmentGrouper grouper(PredefinedFormats::createCoprar00BTransitionTable());

    QList<Group> messages;

    while (reader.peek().tag() == QByteArray("UNH")) {

        Segment unh = reader.peek();
        QCOMPARE(unh.tag(),QByteArray("UNH"));
        QCOMPARE(unh.data(2, 1).asString(), QByteArray("COPRAR"));
        QCOMPARE(unh.data(2, 2).asString(), QByteArray("D"));
        QCOMPARE(unh.data(2, 3).asString(), QByteArray("00B"));
        QCOMPARE(unh.data(2, 4).asString(), QByteArray("UN") );

        Group coprar = grouper.group(&reader);
        QVERIFY(!coprar.isNull());
        messages << coprar;

        QCOMPARE(grouper.errorCode(),0);
    }

    Segment unz = reader.next();
    QCOMPARE(unz.tag(), QByteArray("UNZ"));
    QVERIFY(!reader.hasNext());

    QCOMPARE(messages.size(),1);

    Group message = messages.first();

    QCOMPARE(message.groupList(1).size(),1);
    Group group1 = message.groupList(1).first();

    QVERIFY(!group1.segmentList("RFF").isEmpty());

    QCOMPARE(message.groupList(2).size(),1);

    Group group2 = message.groupList(2).first();
    QVERIFY(!group2.segmentList("TDT").isEmpty());
    QVERIFY(!group2.segmentList("RFF").isEmpty());

    QCOMPARE(group2.groupList(3).size(),1);

    Group group3 = group2.groupList(3).first();

    QVERIFY(!group3.segmentList("LOC").isEmpty());

    QCOMPARE(message.groupList(4).size(), 1);

    QList<Group> group4 = message.groupList(4);

    QVERIFY(!group4.at(0).segmentList("NAD").isEmpty());

    QVERIFY(group4.at(0).groupList(5).isEmpty());


    QCOMPARE(message.groupList(6).size(), 47);

    QList<Group> group6 = message.groupList(6);

    QVERIFY(!group6.at(0).segmentList("EQD").isEmpty());
    QCOMPARE(group6.at(0).segmentList("EQD").first().tag(), QByteArray("EQD"));
    QCOMPARE(group6.at(0).segmentList("RFF").size(),1);
    QCOMPARE(group6.at(0).segmentList("EQN").size(),0);
    QCOMPARE(group6.at(0).segmentList("TMD").size(),0);
    QCOMPARE(group6.at(0).segmentList("DTM").size(),0);
    QCOMPARE(group6.at(0).segmentList("LOC").size(),4);
    QCOMPARE(group6.at(0).segmentList("MEA").size(),1);
    QCOMPARE(group6.at(0).segmentList("DIM").size(),0);
    QCOMPARE(group6.at(0).groupList(7).size(),0);
    QCOMPARE(group6.at(0).segmentList("SEL").size(),1);
    QCOMPARE(group6.at(0).segmentList("FTX").size(),0);
    QCOMPARE(group6.at(0).groupList(8).size(),0);
    QCOMPARE(group6.at(0).segmentList("EQA").size(),0);
    QCOMPARE(group6.at(0).segmentList("HAN").size(),0);
    QCOMPARE(group6.at(0).groupList(10).size(),0);
    QCOMPARE(group6.at(0).segmentList("NAD").size(),1);

    QVERIFY(!group6.at(29).segmentList("EQD").isEmpty());
    QCOMPARE(group6.at(29).segmentList("EQD").first().tag(), QByteArray("EQD"));
    QCOMPARE(group6.at(29).segmentList("RFF").size(),1);
    QCOMPARE(group6.at(29).segmentList("EQN").size(),0);
    QCOMPARE(group6.at(29).segmentList("TMD").size(),0);
    QCOMPARE(group6.at(29).segmentList("DTM").size(),0);
    QCOMPARE(group6.at(29).segmentList("LOC").size(),4);
    QCOMPARE(group6.at(29).segmentList("MEA").size(),1);
    QCOMPARE(group6.at(29).segmentList("DIM").size(),0);
    QCOMPARE(group6.at(29).groupList(7).size(),0);
    QCOMPARE(group6.at(29).segmentList("SEL").size(),1);
    QCOMPARE(group6.at(29).segmentList("FTX").size(),0);
    QCOMPARE(group6.at(29).groupList(8).size(),1);
    QCOMPARE(group6.at(29).groupList(8).first().segmentList("DGS").size(),1);

    QCOMPARE(group6.at(29).segmentList("EQA").size(),0);
    QCOMPARE(group6.at(29).segmentList("HAN").size(),0);
    QCOMPARE(group6.at(29).groupList(10).size(),0);
    QCOMPARE(group6.at(29).segmentList("NAD").size(),1);

    QVERIFY(!group6.at(46).segmentList("EQD").isEmpty());
    QCOMPARE(group6.at(46).segmentList("EQD").first().tag(), QByteArray("EQD"));
    QCOMPARE(group6.at(46).segmentList("RFF").size(),1);
    QCOMPARE(group6.at(46).segmentList("EQN").size(),0);
    QCOMPARE(group6.at(46).segmentList("TMD").size(),0);
    QCOMPARE(group6.at(46).segmentList("DTM").size(),0);
    QCOMPARE(group6.at(46).segmentList("LOC").size(),4);
    QCOMPARE(group6.at(46).segmentList("MEA").size(),1);
    QCOMPARE(group6.at(46).segmentList("DIM").size(),0);
    QCOMPARE(group6.at(46).groupList(7).size(),0);
    QCOMPARE(group6.at(46).segmentList("SEL").size(),0);
    QCOMPARE(group6.at(46).segmentList("FTX").size(),0);
    QCOMPARE(group6.at(46).groupList(8).size(),0);
    QCOMPARE(group6.at(46).segmentList("EQA").size(),0);
    QCOMPARE(group6.at(46).segmentList("HAN").size(),0);
    QCOMPARE(group6.at(46).groupList(10).size(),0);
    QCOMPARE(group6.at(46).segmentList("NAD").size(),1);
}


QTEST_GUILESS_MAIN(Coprar00BReaderTest);
#include "coprar00breadertest.moc"
