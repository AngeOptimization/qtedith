#include <QTest>
#include <QFile>
#include "predefinedformats.h"
#include "segmentgrouper.h"
#include "edifactsegmentreader.h"

using namespace QtEdith;

class Baplie95BReaderTest : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void testBaplie95BGrouper01();
};

void Baplie95BReaderTest::testBaplie95BGrouper01() {
    QString testfile = QFINDTESTDATA("baplie95Bdata/BAPLIE-AAC1-S-AAC1_JUBL_1403_SIN.edi");
    QVERIFY(QFile::exists(testfile));

    QFile f(testfile);
    QVERIFY(f.open(QIODevice::ReadOnly));

    EdifactSegmentReader reader(&f);
    Segment s = reader.next();
    QCOMPARE(s.tag(),QByteArray("UNB"));

    SegmentGrouper grouper(PredefinedFormats::createBaplie95BTransitionTable());

    QList<Group> messages;

    while (reader.peek().tag() == QByteArray("UNH")) {

        Segment unh = reader.peek();
        QCOMPARE(unh.tag(),QByteArray("UNH"));
        QCOMPARE(unh.data(2, 1).asString(), QByteArray("BAPLIE"));
        QCOMPARE(unh.data(2, 2).asString(), QByteArray("D"));
        QCOMPARE(unh.data(2, 3).asString(), QByteArray("95B"));
        QCOMPARE(unh.data(2, 4).asString(), QByteArray("UN") );
        QCOMPARE(unh.data(2, 5).asString(), QByteArray("SMDG20") );

        Group coprar = grouper.group(&reader);
        QVERIFY(!coprar.isNull());
        messages << coprar;

        QCOMPARE(grouper.errorCode(),0);
    }

    Segment unz = reader.next();
    QCOMPARE(unz.tag(), QByteArray("UNZ"));
    QVERIFY(!reader.hasNext());

    QCOMPARE(messages.size(),1);

    Group message = messages.first();

    QVERIFY(!message.segmentList("BGM").isEmpty());
    QVERIFY(!message.segmentList("DTM").isEmpty());
    QVERIFY(message.segmentList("RFF").isEmpty());
    QVERIFY(message.segmentList("NAD").isEmpty());

    QCOMPARE(message.groupList(1).size(),1);

    Group group1 = message.groupList(1).first();

    QVERIFY(!group1.segmentList("TDT").isEmpty());
    QCOMPARE(group1.segmentList("LOC").size(),2);
    QCOMPARE(group1.segmentList("DTM").size(),3);
    QCOMPARE(group1.segmentList("RFF").size(),1);
    QCOMPARE(group1.segmentList("FTX").size(),0);

    QCOMPARE(message.groupList(2).size(),2081);

    QList<Group> group2 = message.groupList(2);

    {
    /*
     * 1st container
     *
     * LOC+147+0590882::5'
     * FTX+ZZZ+++HL'
     * MEA+WT++KGM:4600'
     * LOC+9+SGSIN:139:6'
     * LOC+11+AUBNE:139:6'
     * RFF+BM:1'
     * EQD+CN+GATU0400027+2210+++5'
     * NAD+CA+HL:172:20'
     */
    QCOMPARE(group2.at(0).segmentList("LOC").size(),1);
    QCOMPARE(group2.at(0).segmentList("GID").size(),0);
    QCOMPARE(group2.at(0).segmentList("GDS").size(),0);
    QCOMPARE(group2.at(0).segmentList("FTX").size(),1);
    QCOMPARE(group2.at(0).segmentList("MEA").size(),1);
    QCOMPARE(group2.at(0).segmentList("DIM").size(),0);
    QCOMPARE(group2.at(0).segmentList("TMP").size(),0);
    QCOMPARE(group2.at(0).segmentList("RNG").size(),0);
    QCOMPARE(group2.at(0).segmentList("LOC",2).size(),2);
    QCOMPARE(group2.at(0).segmentList("MEA").size(),1);
    QCOMPARE(group2.at(0).groupList(3).size(),1);

    Group group3 = group2.at(0).groupList(3).first();

    QCOMPARE(group3.segmentList("EQD").size(),1);
    QCOMPARE(group3.segmentList("EQA").size(),0);
    QCOMPARE(group3.segmentList("NAD").size(),1);

    QCOMPARE(group2.at(0).groupList(4).size(),0);
    }
    {
    /*
     * 19th container
     *
     * LOC+147+0100314::5'
     * FTX+ZZZ+++HJ'
     * MEA+WT++KGM:9600'
     * LOC+9+SGSIN:139:6'
     * LOC+11+AUMEL:139:6'
     * RFF+BM:1'
     * EQD+CN+SESU6009948+4310+++5'
     * NAD+CA+HJS:172:20'
     * DGS+IMD+3+1263'
     */
    QCOMPARE(group2.at(18).segmentList("LOC").size(),1);
    QCOMPARE(group2.at(18).segmentList("GID").size(),0);
    QCOMPARE(group2.at(18).segmentList("GDS").size(),0);
    QCOMPARE(group2.at(18).segmentList("FTX").size(),1);
    QCOMPARE(group2.at(18).segmentList("MEA").size(),1);
    QCOMPARE(group2.at(18).segmentList("DIM").size(),0);
    QCOMPARE(group2.at(18).segmentList("TMP").size(),0);
    QCOMPARE(group2.at(18).segmentList("RNG").size(),0);
    QCOMPARE(group2.at(18).segmentList("LOC",2).size(),2);
    QCOMPARE(group2.at(18).segmentList("RFF").size(),1);
    QCOMPARE(group2.at(18).groupList(3).size(),1);

    Group group3 = group2.at(18).groupList(3).first();

    QCOMPARE(group3.segmentList("EQD").size(),1);
    QCOMPARE(group3.segmentList("EQA").size(),0);
    QCOMPARE(group3.segmentList("NAD").size(),1);

    QCOMPARE(group2.at(18).groupList(4).size(),1);

    Group group4 = group2.at(18).groupList(4).first();
    QCOMPARE(group4.segmentList("DGS").size(),1);
    QCOMPARE(group4.segmentList("FTX").size(),0);
    }
}

QTEST_GUILESS_MAIN(Baplie95BReaderTest);
#include "baplie95breadertest.moc"
