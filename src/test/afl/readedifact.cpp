#include "edifactsegmentreader.h"

#include <QDebug>
#include <QFile>
#include <QTextStream>

using namespace QtEdith;

int main () {
    QFile in;
    in.open(stdin, QIODevice::ReadOnly);
    QTextStream out(stdout);

    EdifactSegmentReader reader(&in);

    while (reader.hasNext()) {
        if (reader.errorCode()) {
            qDebug() << "ERROR:" << __LINE__;
            return 1;
        }
        Segment s = reader.next();
        if (reader.errorCode()) {
            qDebug() << "ERROR:" << __LINE__;
            return 2;
        }
        out << s.tag();
        out << " * ";
        out << s.print();
        out << " * ";
        for (int composite = 1; composite <= 3; ++composite) {
            if (composite != 1) {
                out << "+";
            }
            for (int component = 1; component <= 4; ++component) {
                if (component != 1) {
                    out << ":";
                }
                Value data = s.data(composite, component);
                out << QString(data.asString(""));
                out << "/";
                out << data.asDouble(qQNaN());
                out << "/";
                out << data.asInt(-1);
            }
        }
        out << "\n";
    }
    if (reader.errorCode()) {
        qDebug() << "Bad file:" << __LINE__;
        return 0;
    }
    qDebug() << "EXIT:" << __LINE__;
}
