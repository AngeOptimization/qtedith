#!/bin/bash

if [[ $EUID -ne 0 ]];
then
    exec sudo /bin/bash "$0" "$@"
fi

set -x

echo 1 > /proc/sys/kernel/sched_child_runs_first
echo 1 > /proc/sys/kernel/sched_autogroup_enabled

for x in /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor
do
    echo performance > $x
done
