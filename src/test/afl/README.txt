Test the simple reading of EDIFACT segments and extraction of data from the components.

Has been running for 10 days on leons 8 cores in order to get full coverage.
Was tested in non-hardened mode and with ASAN disabled.

Found no bugs.

================================================================================

                      american fuzzy lop 1.56b (fuzzer00)

┌─ process timing ─────────────────────────────────────┬─ overall results ─────┐
│        run time : 10 days, 23 hrs, 0 min, 1 sec      │  cycles done : 45     │
│   last new path : 9 days, 10 hrs, 19 min, 1 sec      │  total paths : 871    │
│ last uniq crash : none seen yet                      │ uniq crashes : 0      │
│  last uniq hang : 0 days, 4 hrs, 6 min, 29 sec       │   uniq hangs : 96     │
├─ cycle progress ────────────────────┬─ map coverage ─┴───────────────────────┤
│  now processing : 290 (33.30%)      │    map density : 862 (1.32%)           │
│ paths timed out : 0 (0.00%)         │ count coverage : 6.03 bits/tuple       │
├─ stage progress ────────────────────┼─ findings in depth ────────────────────┤
│  now trying : splice 1              │ favored paths : 83 (9.53%)             │
│ stage execs : 300/500 (60.00%)      │  new edges on : 106 (12.17%)           │
│ total execs : 241M                  │ total crashes : 0 (0 unique)           │
│  exec speed : 1013/sec              │   total hangs : 510k (96 unique)       │
├─ fuzzing strategy yields ───────────┴───────────────┬─ path geometry ────────┤
│   bit flips : 17/6.14M, 0/6.14M, 0/6.13M            │    levels : 3          │
│  byte flips : 0/767k, 0/705k, 3/721k                │   pending : 0          │
│ arithmetics : 34/36.7M, 0/6.45M, 0/1.45M            │  pend fav : 0          │
│  known ints : 13/4.15M, 4/25.1M, 10/35.4M           │ own finds : 293        │
│  dictionary : 0/0, 0/0, 0/28.6M                     │  imported : 576        │
│       havoc : 198/31.2M, 0/51.4M                    │  variable : 0          │
│        trim : 3.67%/205k, 12.44%                    ├────────────────────────┘
└─────────────────────────────────────────────────────┘             [cpu:124%]


status check tool for afl-fuzz by <lcamtuf@google.com>

Individual fuzzers
==================

>>> fuzzer06 (10 days, 22 hrs) <<<

  cycle 374, lifetime speed 652 execs/sec, path 13/905 (1%)
  pending 0/0, coverage 1.32%, no crashes yet

>>> fuzzer07 (10 days, 22 hrs) <<<

  cycle 412, lifetime speed 675 execs/sec, path 234/894 (26%)
  pending 0/0, coverage 1.32%, no crashes yet

>>> fuzzer05 (10 days, 22 hrs) <<<

  cycle 410, lifetime speed 664 execs/sec, path 82/875 (9%)
  pending 0/0, coverage 1.32%, no crashes yet

>>> fuzzer04 (10 days, 22 hrs) <<<

  cycle 366, lifetime speed 665 execs/sec, path 48/906 (5%)
  pending 0/0, coverage 1.32%, no crashes yet

>>> fuzzer01 (10 days, 22 hrs) <<<

  cycle 391, lifetime speed 667 execs/sec, path 104/892 (11%)
  pending 0/0, coverage 1.32%, no crashes yet

>>> fuzzer03 (10 days, 22 hrs) <<<

  cycle 379, lifetime speed 663 execs/sec, path 190/882 (21%)
  pending 0/0, coverage 1.32%, no crashes yet

>>> fuzzer02 (10 days, 22 hrs) <<<

  cycle 366, lifetime speed 665 execs/sec, path 254/898 (28%)
  pending 0/0, coverage 1.32%, no crashes yet

>>> fuzzer00 (10 days, 22 hrs) <<<

  cycle 46, lifetime speed 254 execs/sec, path 187/871 (21%)
  pending 0/0, coverage 1.32%, no crashes yet

Summary stats
=============

       Fuzzers alive : 8
      Total run time : 87 days, 15 hours
         Total execs : 4647 million
    Cumulative speed : 4909 execs/sec
       Pending paths : 0 faves, 0 total
  Pending per fuzzer : 0 faves, 0 total (on average)
       Crashes found : 0 locally unique
