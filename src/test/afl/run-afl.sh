#!/bin/bash
#set -x
set -e

if [ "$(readlink -f $0)" != "$(readlink -f 'src/test/afl/run-afl.sh')" ]
then
    echo "ERROR: Must be called from qtedith dir!" >&2
    exit 1
fi

if [ ! -e 'build-afl/' ]
then
    echo "ERROR: build-afl/ dir does not exist!" >&2
    exit 2
fi

cd build-afl
cd src/test/afl

(
    sleep 10
    for n in $(seq -f %02.0f 7)
    do
        afl-fuzz -i ../../../../src/test/afl/input -o sync_dir -m 100 -S fuzzer$n -- ./readedifact > sync_dir/fuzzer$n.log &
    done
) &
afl-fuzz -i ../../../../src/test/afl/input -o sync_dir -m 100 -M fuzzer00 -- ./readedifact
