#!/bin/bash
#set -x
set -e

if [ "$(readlink -f $0)" != "$(readlink -f 'src/test/afl/build-afl.sh')" ]
then
    echo "ERROR: Must be called from qtedith dir!" >&2
    exit 1
fi

if [ -e 'build-afl/' ]
then
    echo "ERROR: build-afl/ dir exists!" >&2
    exit 2
fi

mkdir build-afl
cd build-afl

# TODO asan: build with -m32, run with -m800

export AFL_HARDEN=1
CXX=/usr/bin/afl-g++ cmake ..
cd src/test/afl
make
