#ifndef TESTPRETTYPRINTERS_H
#define TESTPRETTYPRINTERS_H
#include <QtTest>
#include "value.h"
#include "token.h"
#include "segment.h"

namespace QTest {
    template<>
    inline char* toString(const QtEdith::Value& value) {
        return QTest::toString<QByteArray>(value.asString());
    }

    template<>
    inline char* toString(const QtEdith::Token& token) {
        if(token.isNull()) {
            return qstrdup("<nulltoken>");
        }
        QByteArray b;
        b.append(token.type());
        b.append(token.value());
        return toString<QByteArray>(b);
    }

    template<>
    inline char* toString(const QtEdith::Segment& segment) {
        return toString<QString>(QString::fromLocal8Bit(segment.print()));
    }
} // namespace QTest

#endif // #TESTPRETTYPRINTERS_H
