#include <QtTest>
#include "transition.h"

using namespace QtEdith;

class TransitionTest : public QObject {
    Q_OBJECT
    private Q_SLOTS:
        void testNullTransition();
        void testSimpleToTransition();
};

void TransitionTest::testNullTransition() {
    Transition t;
    QVERIFY(t.isNull());

    Transition t2 = t;
    QCOMPARE(t,t2);

    Transition t3;
    QCOMPARE(t2,t3);
}

void TransitionTest::testSimpleToTransition() {
    Transition t = Transition::to(0);

    QVERIFY(!t.isNull());

    QCOMPARE(t.state(),0);
    QCOMPARE(t.pop(),0);
}


QTEST_GUILESS_MAIN(TransitionTest);
#include "transitiontest.moc"
