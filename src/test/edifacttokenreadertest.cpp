#include <QtTest>
#include "edifacttokenreader.h"
#include "testprettyprinters.h"

using namespace QtEdith;



class EdifactTokenReaderTest : public QObject {
    Q_OBJECT
    private Q_SLOTS:
        void testSimpleStructure();
        void testNoTerminatorAtEnd();
        void testReleaseAtEnd();
        void testRelease();
};

void EdifactTokenReaderTest::testSimpleStructure() {
    QByteArray array("EQD+CN+ZZZU0000004+42G1+++5'");
    QBuffer buffer(&array);
    buffer.open(QIODevice::ReadOnly);

    EdifactTokenReader reader(&buffer);
    QVERIFY(reader.hasNext());
    QCOMPARE(reader.peek(),Token("EQD"));
    QCOMPARE(reader.peek(),Token("EQD"));
    QCOMPARE(reader.peek(),Token("EQD"));
    QCOMPARE(reader.peek(),Token("EQD"));
    QCOMPARE(reader.next(),Token("EQD"));
    QVERIFY(reader.hasNext());
    QCOMPARE(reader.peek(), Token(Token::DataElementSeparator));
    QCOMPARE(reader.peek(), Token(Token::DataElementSeparator));
    QCOMPARE(reader.peek(), Token(Token::DataElementSeparator));
    QCOMPARE(reader.next(), Token(Token::DataElementSeparator));
    QVERIFY(reader.hasNext());
    QCOMPARE(reader.peek(),Token("CN"));
    QCOMPARE(reader.next(),Token("CN"));
    QVERIFY(reader.hasNext());
    QCOMPARE(reader.peek(), Token(Token::DataElementSeparator));
    QCOMPARE(reader.next(), Token(Token::DataElementSeparator));
    QVERIFY(reader.hasNext());
    QCOMPARE(reader.peek(),Token("ZZZU0000004"));
    QCOMPARE(reader.next(),Token("ZZZU0000004"));
    QVERIFY(reader.hasNext());
    QCOMPARE(reader.peek(), Token(Token::DataElementSeparator));
    QCOMPARE(reader.next(), Token(Token::DataElementSeparator));
    QVERIFY(reader.hasNext());
    QCOMPARE(reader.peek(),Token("42G1"));
    QCOMPARE(reader.next(),Token("42G1"));
    QVERIFY(reader.hasNext());
    QCOMPARE(reader.peek(), Token(Token::DataElementSeparator));
    QCOMPARE(reader.next(), Token(Token::DataElementSeparator));
    QVERIFY(reader.hasNext());
    QCOMPARE(reader.peek(), Token(Token::DataElementSeparator));
    QCOMPARE(reader.next(), Token(Token::DataElementSeparator));
    QVERIFY(reader.hasNext());
    QCOMPARE(reader.peek(), Token(Token::DataElementSeparator));
    QCOMPARE(reader.next(), Token(Token::DataElementSeparator));
    QVERIFY(reader.hasNext());
    QCOMPARE(reader.peek(),Token("5"));
    QCOMPARE(reader.next(),Token("5"));
    QVERIFY(reader.hasNext());
    QCOMPARE(reader.peek(), Token(Token::SegmentTerminator));
    QCOMPARE(reader.next(), Token(Token::SegmentTerminator));
    QVERIFY(!reader.hasNext());
    QCOMPARE(reader.errorCode(), int(EdifactTokenReader::NoError));
    QCOMPARE(reader.peek(), Token());
    QCOMPARE(reader.next(), Token());
}

void EdifactTokenReaderTest::testNoTerminatorAtEnd() {
    QByteArray array("EQD+A");
    QBuffer buffer(&array);
    buffer.open(QIODevice::ReadOnly);

    EdifactTokenReader reader(&buffer);
    QVERIFY(reader.hasNext());
    QCOMPARE(reader.peek(),Token("EQD"));
    QCOMPARE(reader.next(),Token("EQD"));
    QVERIFY(reader.hasNext());
    QCOMPARE(reader.peek(), Token(Token::DataElementSeparator));
    QCOMPARE(reader.next(), Token(Token::DataElementSeparator));
    QVERIFY(reader.hasNext());
    QCOMPARE(reader.peek(),Token("A"));
    QCOMPARE(reader.errorCode(), int(EdifactTokenReader::NoTerminatorAtEOF));
    QCOMPARE(reader.next(),Token("A"));
    QVERIFY(!reader.hasNext());
}

void EdifactTokenReaderTest::testReleaseAtEnd() {
    QByteArray array("EQD+A?");
    QBuffer buffer(&array);
    buffer.open(QIODevice::ReadOnly);

    EdifactTokenReader reader(&buffer);
    QVERIFY(reader.hasNext());
    QCOMPARE(reader.peek(),Token("EQD"));
    QCOMPARE(reader.next(),Token("EQD"));
    QVERIFY(reader.hasNext());
    QCOMPARE(reader.peek(), Token(Token::DataElementSeparator));
    QCOMPARE(reader.next(), Token(Token::DataElementSeparator));
    QVERIFY(!reader.hasNext());
    QCOMPARE(reader.peek(),Token());
    QCOMPARE(reader.errorCode(), int(EdifactTokenReader::ReleaseCharAtEOF));
    QCOMPARE(reader.next(),Token());
    QVERIFY(!reader.hasNext());
}

void EdifactTokenReaderTest::testRelease() {
    QByteArray array("EQD+A?+'");
    QBuffer buffer(&array);
    buffer.open(QIODevice::ReadOnly);

    EdifactTokenReader reader(&buffer);
    QVERIFY(reader.hasNext());
    QCOMPARE(reader.peek(),Token("EQD"));
    QCOMPARE(reader.next(),Token("EQD"));
    QVERIFY(reader.hasNext());
    QCOMPARE(reader.peek(), Token(Token::DataElementSeparator));
    QCOMPARE(reader.next(), Token(Token::DataElementSeparator));
    QVERIFY(reader.hasNext());
    QCOMPARE(reader.peek(),Token("A+"));
    QCOMPARE(reader.next(),Token("A+"));
    QVERIFY(reader.hasNext());
    QCOMPARE(reader.errorCode(), int(EdifactTokenReader::NoError));

}

QTEST_GUILESS_MAIN(EdifactTokenReaderTest);
#include "edifacttokenreadertest.moc"
