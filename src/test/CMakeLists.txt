find_package(Qt5Test 5.2.0 REQUIRED CONFIG)

macro(make_test testbasename)
    add_executable(${testbasename} ${testbasename}.cpp)
    target_link_libraries(${testbasename}
        Qt5::Core
        Qt5::Test
        QtEdith
    )
    add_test(${testbasename} ${testbasename})
endmacro(make_test)

make_test(baplie911readertest)
make_test(baplie95breadertest)
make_test(baplie22readertest)
make_test(coprar00breadertest)
make_test(edifactsegmentreadertest)
make_test(edifacttokenreadertest)
make_test(segmenttest)
make_test(transitiontest)
make_test(valuetest)

add_subdirectory(afl)
add_subdirectory(afl2)
