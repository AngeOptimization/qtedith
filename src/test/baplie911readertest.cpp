#include <QTest>
#include <QFile>

#include "edifactsegmentreader.h"
#include "predefinedformats.h"
#include "segmentgrouper.h"

using namespace QtEdith;

class Baplie911ReaderTest : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void testBaplie911Grouper01();
};

void Baplie911ReaderTest::testBaplie911Grouper01() {
    QString testfile = QFINDTESTDATA("baplie911data/BAPLIE-JAS1--HUZM_1410W_DEP_HKT2.edi");
    QVERIFY(QFile::exists(testfile));

    QFile file(testfile);
    QVERIFY(file.open(QIODevice::ReadOnly));

    EdifactSegmentReader reader(&file);

    Segment unb = reader.next();
    QCOMPARE(unb.tag(), QByteArray("UNB"));

    // Can use BAPLIE 95B table with this BAPLIE 911
    SegmentGrouper grouper(PredefinedFormats::createBaplie95BTransitionTable());

    Segment unh = reader.peek();
    QCOMPARE(unh.tag(),QByteArray("UNH"));
    QCOMPARE(unh.data(2, 1).asString(), QByteArray("BAPLIE"));
    QCOMPARE(unh.data(2, 2).asString(), QByteArray("1"));
    QCOMPARE(unh.data(2, 3).asString(), QByteArray("911"));
    QCOMPARE(unh.data(2, 4).asString(), QByteArray("UN") );
    QCOMPARE(unh.data(2, 5).asString(), QByteArray("SMDG15") );

    Group baplie = grouper.group(&reader);
    QVERIFY(!baplie.isNull());
    QCOMPARE(grouper.errorCode(),0);

    Segment unz = reader.next();
    QCOMPARE(unz.tag(), QByteArray("UNZ"));
    QVERIFY(!reader.hasNext());

    // There is an error in the file, test that it is found
    QCOMPARE(reader.errorCode(), 11);  // 11 is error when reading the DataElementSeparator after the tag
}

QTEST_GUILESS_MAIN(Baplie911ReaderTest);

#include "baplie911readertest.moc"
