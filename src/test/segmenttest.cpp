#include <QtTest>
#include <QtTest/QTest>

#include "segment.h"
#include "testprettyprinters.h"

using namespace QtEdith;



class SegmentTest : public QObject {
    Q_OBJECT
    private Q_SLOTS:
        void testSegment();
        void testBuilder();
};

void SegmentTest::testSegment() {
    SegmentBuilder builder("UNH");
    builder.addValue("123456789");
    builder.nextDataElement();
    builder.addValue("IFTSAI");
    builder.addValue("D");
    builder.addValue("99B");
    builder.addValue("UN");
    Segment unh = builder.build();

    QCOMPARE(unh.print(),QByteArray("UNH+123456789+IFTSAI:D:99B:UN'"));

    QCOMPARE(unh.tag(), QByteArray("UNH"));
    QVERIFY(unh.tag() != QByteArray("WRONG") );

    QCOMPARE(unh.data(1,1), Value::valueOf("123456789"));
    QVERIFY(Value::valueOf("WRONG") != unh.data(1, 1));
    QCOMPARE(unh.data(2,1), Value::valueOf("IFTSAI"));
    QCOMPARE(unh.data(2,2), Value::valueOf("D"));
    QCOMPARE(unh.data(2,3), Value::valueOf("99B"));
    QCOMPARE(unh.data(2,4), Value::valueOf("UN"));

    // the following two composites do not exist in this segment
    QVERIFY(!unh.data(2, 5).isPresent());
    QVERIFY(!unh.data(3, 1).isPresent());
}

void SegmentTest::testBuilder() {
    SegmentBuilder builder("UNH");
    builder.addValue("123456789");
    builder.nextDataElement();
    builder.addValue("IFTSAI");
    builder.addValue("D");
    builder.addValue("99B");
    builder.addValue("UN");
    Segment unh = builder.build();
    Segment unh2 = builder.build();
    builder.nextDataElement();
    builder.addValue("HELLO"); // shared data here should be detached
    Segment unhExtra = builder.build();
    QCOMPARE(unh, unh2);
    QVERIFY(unh != unhExtra);
}



QTEST_GUILESS_MAIN(SegmentTest)

#include "segmenttest.moc"
