#include "transition.h"

#include <QSharedData>

using namespace QtEdith;

class QtEdith::TransitionData : public QSharedData {
public:
    TransitionData(int state, bool createNewGroup, int pop, int occurrence)
        : m_state(state), m_createNewGroup(createNewGroup), m_pop(pop), m_occurrence(occurrence)
    {
        // Empty
    }
    bool operator==(const TransitionData& other) const {
        return m_state == other.m_state
            && m_createNewGroup == other.m_createNewGroup
            && m_pop == other.m_pop
            && m_occurrence == other.m_occurrence;
    }
public:
    int m_state;
    int m_createNewGroup;
    int m_pop;
    int m_occurrence;
};

Transition::Transition() : d(0) {
    // Empty
}

Transition::Transition(int state, bool createNewGroup, int pop, int occurrence)
    : d(new TransitionData(state, createNewGroup, pop, occurrence))
{
    // Empty
}

Transition::Transition(const Transition& other) : d(other.d) {
    // Empty
}

Transition::~Transition() {
    // Empty
}

Transition& Transition::operator=(const Transition& other) {
    this->d = other.d;
    return *this;
}

bool Transition::operator==(const Transition& other) const {
    if(this->d == other.d) {
        return true;
    }
    if(this->d && other.d) {
        return *(this->d) == *(other.d);
    }
    return false;
}

bool Transition::isNull() const {
    return d == 0;
}

Transition Transition::to(int state) {
    return Transition(state, state > 0, 0, 1);
}

Transition Transition::createNewGroup(bool newCreateNewGroup) const {
    Q_ASSERT(d);
    return Transition(d->m_state, newCreateNewGroup, d->m_pop, d->m_occurrence);
}

Transition Transition::pop(int newPop) const {
    Q_ASSERT(d);
    return Transition(d->m_state, d->m_createNewGroup, newPop, d->m_occurrence);
}

Transition Transition::occurrence(int newOccurrence) const {
    Q_ASSERT(d);
    return Transition(d->m_state, d->m_createNewGroup, d->m_pop, newOccurrence);
}

int Transition::state() const {
    Q_ASSERT(d);
    return d->m_state;
}

bool Transition::createNewGroup() const {
    Q_ASSERT(d);
    return d->m_createNewGroup;
}

int Transition::groupNumber() const {
    Q_ASSERT(d);
    Q_ASSERT(d->m_state >= 0);
    Q_ASSERT(d->m_createNewGroup);
    return d->m_state;
}

int Transition::pop() const {
    Q_ASSERT(d);
    return d->m_pop;
}

int Transition::occurrence() const {
    Q_ASSERT(d);
    return d->m_occurrence;
}
