#ifndef QTEDITH_TRANSITIONTABLE_H
#define QTEDITH_TRANSITIONTABLE_H

#include <QSharedDataPointer>
#include "qtedith_export.h"
#include "transition.h"

namespace QtEdith {

class TransitionTableData;

/**
 * A table describing the transitions needed for a given format
 * A table is immutable
 */
class QTEDITH_EXPORT TransitionTable {
    public:
        TransitionTable(const TransitionTable& other);
        ~TransitionTable();
        TransitionTable& operator=(const TransitionTable& other);
        /**
         * \param tag
         * \param state
         * \return true if there is a transition for the given state and tag
         */
        bool contains(const QByteArray& tag, int state) const;
        /**
         * \return the transition for the given \param tag and \param state
         * Will return an invalid Transition if not found.
         */
        QtEdith::Transition value(const QByteArray& tag, int state) const;
    private:
        QSharedDataPointer<TransitionTableData> d;
        friend class TransitionTableBuilder;
        /**
         * Constructor for the builder
         */
        TransitionTable(TransitionTableData* data);
};

/**
 * Builder for a TransitionTable
 */
class QTEDITH_EXPORT TransitionTableBuilder {
    public:
        TransitionTableBuilder();
        ~TransitionTableBuilder();
        /**
         * Adds a transition with a given tag and state
         */
        void add(QByteArray tag, int state, const QtEdith::Transition& transition);
        /**
         * Builds the Transition table with the current internal state of the builder
         */
        TransitionTable build();
    private:
        Q_DISABLE_COPY(TransitionTableBuilder);
        QSharedDataPointer<TransitionTableData> d; // shared with the Transition table
};
}

#endif // QTEDITH_TRANSITIONTABLE_H
