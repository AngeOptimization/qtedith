#include "token.h"

#include <QByteArray>
#include <QSharedData>

using namespace QtEdith;

class QtEdith::TokenData : public QSharedData {
public:
    TokenData(Token::Type type) : m_type(type) {
        // Empty
    }
    Token::Type m_type;
    QByteArray m_value;
    bool operator==(const TokenData& other) const {
        return m_type == other.m_type && m_value == other.m_value;
    }
};

Token::Token() : d(0) {
    // Empty
}

Token::Token(const Token& other) : d(other.d) {
    // Empty
}

Token::Token(Token::Type type)
  : d(new TokenData(type))  // find some way to actually share the TokenData's in this case.
{
    // Empty
}

Token::~Token() {
    // Empty
}

bool Token::isNull() const {
    return d == 0;
}

Token::Type Token::type() const {
    Q_ASSERT(d);
    return d->m_type;
}

QByteArray Token::value() const {
    Q_ASSERT(d);
    return d->m_value;
}

Token& Token::operator=(const Token& other) {
    this->d = other.d;
    return *this;
}

bool Token::operator==(const Token& other) const {
    if (this->d == other.d) {
        return true;
    }
    if (this->d && other.d) {
        return *(this->d) == *(other.d);
    }
    return false;
}

bool Token::operator!=(const Token& other) const {
    return !(*this == other);
}

Token::Token(const QByteArray& array) : d(new TokenData(Value)) {
    d->m_value = array;
}
