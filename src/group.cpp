#include "group.h"
#include <QSharedData>
#include <QMultiHash>
#include <qtextstream.h>
#include "tagoccurrence_p.h"

using namespace QtEdith;
using namespace QtEdithPrivate;

class QtEdith::GroupBuilderData : public QSharedData {
    public:
        QMultiHash<TagOccurence, Segment> m_segments;
        QMultiHash<int, GroupBuilder*> m_groups;
        ~GroupBuilderData() {
            qDeleteAll(m_groups.values());
        }
};

class QtEdith::GroupData : public QSharedData {
    public:
        GroupData() {
        }
        GroupData(GroupBuilderData* builderdata) : m_segments(builderdata->m_segments) {
            for(QMultiHash<int, GroupBuilder*>::iterator it = builderdata->m_groups.begin();
                it != builderdata->m_groups.end() ; ++it) {
                m_groups.insertMulti(it.key(), it.value()->build());
            }
            Q_ASSERT(m_groups.size() == builderdata->m_groups.size());
            Q_ASSERT(m_segments.size() == builderdata->m_segments.size());
        }
        QMultiHash<TagOccurence, Segment> m_segments;
        QMultiHash<int, Group> m_groups;
        QString debugString() {
            QString result;
            QTextStream stream(&result);
            stream << "Segments: " << m_segments.count() << "\n";
            stream << "Groups: " << m_groups.count() << "\n";
            for(QMultiHash<TagOccurence, Segment>::iterator iterator = m_segments.begin(); iterator != m_segments.end(); ++iterator) {
                stream << iterator.key().first << "," << iterator.key().second;
                stream << ":";
                stream <<  iterator.value().print();
                stream << "\n";
            }
            for(QMultiHash<int, Group>::iterator iterator = m_groups.begin(); iterator!= m_groups.end(); ++iterator) {
                stream <<  "Group ";
                stream << iterator.key();
                stream << "\n";
                stream << iterator.value().debugString();
            }
            return result;
        }
};

Group::Group() : d(0) {
}

bool Group::isNull() const {
    return d==0;
}

Group::Group(GroupBuilderData* data) : d(new GroupData(data)) {

}

Group::Group(const Group& other) : d(other.d) {

}

QList< Group > Group::groupList(int groupNumber) const {
    return d->m_groups.values(groupNumber);
}

Group& Group::operator=(const Group& other) {
    this->d = other.d;
    return *this;
}

QList< Segment > Group::segmentList(const QByteArray& tag, int occurrence) const {
    return d->m_segments.values(TagOccurence(tag,occurrence));
}

Group::~Group() {

}

void GroupBuilder::add(int groupNumber, GroupBuilder* groupbuilder) {
    d->m_groups.insertMulti(groupNumber, groupbuilder);
}

void GroupBuilder::add(const Segment& segment, int occurrence) {
    Q_ASSERT(!segment.isNull());
    d->m_segments.insert(TagOccurence(segment.tag(), occurrence), segment);
}

Group GroupBuilder::build() {
    return Group(d.data());
}

GroupBuilder::GroupBuilder() : d(new GroupBuilderData()) {

}

GroupBuilder::~GroupBuilder() {

}

QString Group::debugString() {
    return d->debugString();
}



