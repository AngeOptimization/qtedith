#ifndef QTEDITH_TRANSITION_H
#define QTEDITH_TRANSITION_H

#include <QSharedDataPointer>
#include "qtedith_export.h"

namespace QtEdith {

class TransitionData;

/**
 * Describe a transition in the parser used in Grouper
 * A transition is immutable
 */
class QTEDITH_EXPORT Transition {

public:
    /**
     * Creates an invalid transition. Needed by QHash.
     */
    Transition();

    /**
     * Copy constructor
     */
    Transition(const Transition& other);

    /**
     * Destructor
     */
    ~Transition();

    /**
     * Assignment operator
     */
    Transition& operator=(const Transition& other);

    /**
     * Equality comparison
     */
    bool operator==(const Transition& other) const;

    /**
     * \true if the transition is created with the default constructor
     */
    bool isNull() const;

    /**
     * \param state
     * \return A new Transition that will change state to \param state. Will not pop any groups, the occurrence
     *         number is 1. It will create a new group if the state is positive.
     */
    static Transition to(int state);

    /**
     * \param newCreateNewGroup
     * \return a new Transition that will is similar to the old but will create new group as ordered in \param newCreateNewGroup
     */
    Transition createNewGroup(bool newCreateNewGroup) const;

    /**
     * \param newPop
     * \return a new Transition that will is similar to the old but will pop \param newPop groups
     */
    Transition pop(int newPop) const;

    /**
     * \param newOccurrence
     * \return a new Transition that will is similar to the old but with change occurrence number to \param newOccurrence
     */
    Transition occurrence(int newOccurrence) const;

    /**
     * \return the state to change to
     */
    int state() const;

    /**
     * \return if a new group should be created
     */
    bool createNewGroup() const;

    /**
     * \return groupNumber to change to after this transition
     */
    int groupNumber() const;

    /**
     * \return the number of groups to pop
     */
    int pop() const;

    /**
     * \return the occurance
     */
    int occurrence() const;

private:
    Transition(int state, bool createNewGroup, int pop, int occurrence);
    QSharedDataPointer<TransitionData> d;
};
    
}

#endif // QTEDITH_TRANSITION_H
