set(SRC
    edifactsegmentreader.cpp
    edifacttokenreader.cpp
    group.cpp
    predefinedformats.cpp
    segment.cpp
    segmentgrouper.cpp
    token.cpp
    transition.cpp
    transitiontable.cpp
    value.cpp
)
add_library(QtEdith SHARED ${SRC})
target_link_libraries(QtEdith
    Qt5::Core
)

add_library(QtEdith_static STATIC ${SRC})
target_link_libraries(QtEdith_static
    Qt5::Core
)
set_target_properties(QtEdith_static PROPERTIES
    COMPILE_FLAGS -DQTEDITH_STATIC_DEFINE  # Needed for export headers for both shared and static library
)

generate_export_header(QtEdith)

set_property(TARGET QtEdith PROPERTY VERSION "${SO_VERSION}.0.0")
set_property(TARGET QtEdith PROPERTY SOVERSION "${SO_VERSION}")

install(TARGETS QtEdith EXPORT QtEdithTargets
    RUNTIME DESTINATION "bin"
    LIBRARY DESTINATION "lib"
    ARCHIVE DESTINATION "lib"
    INCLUDES DESTINATION "include"
)

install(FILES
    ${CMAKE_CURRENT_BINARY_DIR}/qtedith_export.h
    edifactsegmentreader.h
    edifacttokenreader.h
    group.h
    predefinedformats.h
    segment.h
    segmentgrouper.h
    token.h
    transition.h
    transitiontable.h
    value.h
    DESTINATION "include/QtEdith"
)

write_basic_package_version_file(
    ${CMAKE_CURRENT_BINARY_DIR}/QtEdithConfigVersion.cmake
    COMPATIBILITY SameMajorVersion
)

configure_file(
    QtEdithConfig.cmake.in
    QtEdithConfig.cmake
    @ONLY
)

set(ConfigPackageLocation lib/cmake/QtEdith)
install(EXPORT QtEdithTargets
    FILE QtEdithTargets.cmake
    DESTINATION ${ConfigPackageLocation}
)
install(FILES
    ${CMAKE_CURRENT_BINARY_DIR}/QtEdithConfigVersion.cmake
    ${CMAKE_CURRENT_BINARY_DIR}/QtEdithConfig.cmake
    DESTINATION ${ConfigPackageLocation}
)

add_subdirectory(test)
