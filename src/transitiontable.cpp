#include "transitiontable.h"

#include <QByteArray>
#include <QHash>
#include <QPair>
#include <QSharedData>

using namespace QtEdith;

typedef QPair<QByteArray, int> TagState;

class QtEdith::TransitionTableData : public QSharedData {
public:
    QHash<TagState, Transition> m_transitions;
};

TransitionTable::TransitionTable(TransitionTableData* data) : d(data) {
    // Empty
}

TransitionTable::TransitionTable(const TransitionTable& other) : d(other.d) {
    // Empty
}

TransitionTable::~TransitionTable() {
    // Empty
}

bool TransitionTable::contains(const QByteArray& tag, int state) const {
    return d->m_transitions.contains(TagState(tag, state));
}

QtEdith::Transition TransitionTable::value(const QByteArray& tag, int state) const {
    return d->m_transitions.value(TagState(tag,state));
}

TransitionTable& TransitionTable::operator=(const TransitionTable& other) {
    this->d = other.d;
    return *this;
}

TransitionTableBuilder::TransitionTableBuilder() : d(new TransitionTableData()) {
    // Empty
}

TransitionTableBuilder::~TransitionTableBuilder() {
    // Empty
}

void TransitionTableBuilder::add(QByteArray tag, int state, const QtEdith::Transition& transition) {
    Q_ASSERT(!transition.isNull());
    d->m_transitions.insert(TagState(tag,state), transition);
}

TransitionTable TransitionTableBuilder::build() {
    return TransitionTable(d);
}
