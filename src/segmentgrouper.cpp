#include "segmentgrouper.h"

#include "edifactsegmentreader.h"
#include "group.h"
#include "transitiontable.h"

#include <QStack>
#include <QString>

using namespace QtEdith;

class QtEdith::SegmentGrouperPrivate {
public:
    SegmentGrouperPrivate(const TransitionTable& table) : m_transitionTable(table), m_error(0) {
        // Empty
    }
    const TransitionTable m_transitionTable;
    int m_error;
    QString m_errorString;
};

int SegmentGrouper::errorCode() {
    return d->m_error;
}

const QString& SegmentGrouper::errorMessage() {
    return d->m_errorString;
}

SegmentGrouper::SegmentGrouper(const TransitionTable& transitionTable)
  : d(new SegmentGrouperPrivate(transitionTable))
{
    // Empty
}

SegmentGrouper::~SegmentGrouper() {
    // Empty
}

Group SegmentGrouper::group(QtEdith::EdifactSegmentReader* reader) {
    GroupBuilder group0Builder;

    if(!reader->hasNext()) {
        d->m_error = 2;
        d->m_errorString = QStringLiteral("End of file ?");
        return Group();
    }
    Segment unh = reader->peek();
    if(unh.tag() != QByteArray("UNH")) {
        d->m_error = 1;
        d->m_errorString = QString("Expected UNH, got %1").arg(QString::fromLocal8Bit(unh.tag()));
        return Group();
    }

    reader->next(); // pop element peek'ed;
    group0Builder.add(unh);

    QStack<GroupBuilder*> stack;
    stack.push(&group0Builder);
    int state = 0;
    while(reader->hasNext()) {
        Segment segment = reader->next();
        QByteArray tag = segment.tag();
        if(tag == QByteArray("UNT")) {
            group0Builder.add(segment);
            break;
        }
        if(d->m_transitionTable.contains(tag,state)) {
            Transition transition = d->m_transitionTable.value(tag,state);
            Q_ASSERT(!transition.isNull());
            for(int i = 0 ; i < transition.pop(); ++i) {
                stack.pop();
            }
            if(transition.createNewGroup()) {
                GroupBuilder* newGroupBuilder = new GroupBuilder();
                stack.last()->add(transition.groupNumber(), newGroupBuilder);
                stack.push(newGroupBuilder);
            }
            state = transition.state();
            stack.last()->add(segment, transition.occurrence());
        } else {
            stack.last()->add(segment);
        }
    }
    Q_ASSERT(!stack.isEmpty());
    Q_ASSERT(stack.first() == &group0Builder);
    return group0Builder.build();
}
