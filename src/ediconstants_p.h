#ifndef EDICONSTANTS_H
#define EDICONSTANTS_H

// internal
#define COMPONENT_DATA_ELEMENT_SEPARATOR ':'
#define DATA_ELEMENT_SEPARATOR '+'
#define RELEASE_CHARACTER '?'
#define SEGMENT_TERMINATOR '\''

namespace QtEdithPrivate {

/**
 * Checks if \param c is one of the predefined edifact characters
 */
inline bool isEdiChar(char c) {
    return c == COMPONENT_DATA_ELEMENT_SEPARATOR || c == DATA_ELEMENT_SEPARATOR || c == RELEASE_CHARACTER || c == SEGMENT_TERMINATOR;
}
} // namespace

#endif // EDICONSTANTS_H
