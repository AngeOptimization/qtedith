#ifndef QTEDITH_EDIFACTSEGMENTREADER_H
#define QTEDITH_EDIFACTSEGMENTREADER_H

#include "qtedith_export.h"

#include "segment.h"

#include <QScopedPointer>

class QIODevice;
namespace QtEdith {
class EdifactSegmentReaderPrivate;
}

namespace QtEdith {

/**
 * Collects the scanned tokens into Segments
 */
class QTEDITH_EXPORT EdifactSegmentReader {
    public:
        /**
         * Constructs a EdifactSegmentReader
         * \param device to read from
         * Device is expected to be open and able to read from
         */
        EdifactSegmentReader(QIODevice* device);
        ~EdifactSegmentReader();
        /**
         * Peeks the next segment
         */
        Segment peek();
        /**
         * Pops the next segment and returns it
         */
        Segment next();
        /**
         * return true if next or peek is expected to give a valid segment
         */
        bool hasNext();
        /**
         * Error code for reader. 0 means no error.
         */
        int errorCode() const;
    private:
        QScopedPointer<EdifactSegmentReaderPrivate> d;
};

}

#endif // QTEDITH_EDIFACTSEGMENTREADER_H
