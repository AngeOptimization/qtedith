#ifndef QTEDITH_SEGMENTGROUPER_H
#define QTEDITH_SEGMENTGROUPER_H
#include "group.h"
#include "qtedith_export.h"

namespace QtEdith {

class EdifactSegmentReader;
class TransitionTable;
class SegmentGrouperPrivate;
/**
 * Groups the segments into groups from a \param EdifactSegmentReader
 */
class QTEDITH_EXPORT SegmentGrouper {
    public:
        /**
         * Creates a SegmentGrouper with a given \link TransitionTable for the transitions
         */
        SegmentGrouper(const QtEdith::TransitionTable& transitionTable);
        /**
         * Will read a BAPLIE message from the event reader.
         *
         * Expects the next event to be a UNH with the correct identifier. If the next event is wrong an error will
         * reported, the method will return an empty Group and no event will be consumed. If the BAPLIE reading start it
         * will continue until the message is ended with an UNT or the event reader has no more events.
         *
         * \param reader
         * \return the entire BAPLIE as a Group
         */
        QtEdith::Group group(QtEdith::EdifactSegmentReader* reader);
        /**
         * \return error code. 0 for means no error
         */
        int errorCode(); // FIXME should be const
        /**
         * \return a human presentable error message
         */
        const QString& errorMessage(); // FIXME should be const
        ~SegmentGrouper();
    private:
        QScopedPointer<SegmentGrouperPrivate> d;
};
}

#endif // QTEDITH_SEGMENTGROUPER_H
