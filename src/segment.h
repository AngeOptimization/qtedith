#ifndef QTEDITH_SEGMENT_H
#define QTEDITH_SEGMENT_H

#include <QSharedDataPointer>
#include "value.h"
#include "qtedith_export.h"


namespace QtEdith {

class SegmentData;
class SegmentBuilder;

/**
 * A segment, i.e. a tag and some \link Value.
 *
 * Immutable.
 */
class QTEDITH_EXPORT Segment {
    public:
        /**
         * Creates an invalid segment. Tag, data and print isn't expected to give something useful
         */
        Segment();
        Segment(const Segment& other);
        ~Segment();
        /**
         * \return true if created with the default constructor
         */
        bool isNull() const;
        Segment& operator=(const Segment& other);
        bool operator==(const Segment& other) const;
        bool operator!=(const Segment& other) const;
        /**
         * The tag for the given Segment.
         * Behaviour is undefined if null segment.
         */
        QByteArray tag() const;
        /**
         * \param compositePosition
         *            1 based position
         * \param componentPosition
         *            1 based sub position
         * \return the data element, if there is no data present the non-present data element will be returned
         * Behaviour is undefined if called on a null segment
         */
        QtEdith::Value data(int compositePosition, int componentComposition) const;
        /**
         * \return raw text of segment
         * Behaviour is undefined if called on a null segment
         */
        QByteArray print() const;
    private:
        /**
         * Constructor for the builder class
         */
        Segment(SegmentData* data);
        QSharedDataPointer<SegmentData> d;
        friend class SegmentBuilder;
};

/**
 * Builder for \link Segment class
 */
class QTEDITH_EXPORT SegmentBuilder {
    public:
        /**
         * Creates a SegmentBuilder for a \param tag
         */
        SegmentBuilder(const QByteArray& tag);
        ~SegmentBuilder();
        /**
         * Adds a new data element
         * \return this for chaining
         */
        SegmentBuilder& nextDataElement();
        /**
         * adds a new value.
         * \return this for chaining
         */
        SegmentBuilder& addValue(const QByteArray& array);
        /**
         * constructs a Segment from this builder
         */
        Segment build();
    private:
        Q_DISABLE_COPY(SegmentBuilder);
        QSharedDataPointer<SegmentData> d; // shared with the Segment
        friend class Segment;
};
} // namespace

#endif // QTEDITH_SEGMENT_H
