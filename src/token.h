#ifndef QTEDITH_TOKEN_H
#define QTEDITH_TOKEN_H

#include <QSharedDataPointer>
#include "qtedith_export.h"

namespace QtEdith {

class TokenData;

/**
 * A token in the EDIFACT file, can be a control char or a string where the release chars has been parsed
 */
class QTEDITH_EXPORT Token {
    public:
        /**
         * Various types of tokens
         */
        enum Type {
            /**
             * Value type
             */
            Value,
            /**
             * Token for the primary separator in a segment, normally a : (colon)
             */
            ComponentDataElementSeparator,
            /**
             * Token for the secondary separator in a segment, normally a + (plus)
             */
            DataElementSeparator,
            /**
             * Token for the separator between segments, normally a ' (apostrophe)
             */
            SegmentTerminator
        };
        /**
         * Creates a invalid/null token
         */
        Token();
        /**
         * \return true if it is a invalid/null token
         */
        bool isNull() const;
        /**
         * Creates a Value token with data from \param array
         */
        Token(const QByteArray& array);
        /**
         * Creates a token of type \param type. Should not be used for value tokens.
         */
        Token(Type type);
        /**
         * Copy ctor
         */
        Token(const Token& other);
        /**
         * \return type for this token
         */
        Type type() const;
        /**
         * \return value for this token
         */
        QByteArray value() const;
        ~Token();
        Token& operator=(const Token& other);
        bool operator==(const Token& other) const;
        bool operator!=(const Token& other) const;
    private:
        QSharedDataPointer<TokenData> d;
};
}

#endif // QTEDITH_TOKEN_H
