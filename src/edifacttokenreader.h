#ifndef QTEDITH_EDIFACTTOKENREADER_H
#define QTEDITH_EDIFACTTOKENREADER_H

#include "qtedith_export.h"
#include "token.h"

#include <QScopedPointer>

class QIODevice;

namespace QtEdith {

class EdifactTokenReaderPrivate;

/**
 * Scans the EDIFACT file and returns a stream of simple tokens
 */
class QTEDITH_EXPORT EdifactTokenReader {

public:

    /**
     * Known error codes
     */
    enum ErrorCodes {
        NoError = 0,
        ReleaseCharAtEOF=10,
        NoTerminatorAtEOF=20
    };

    /**
     * Constructs a tokenreader.
     * \param device to read from
     * Device is expected to be open and readable
     */
    EdifactTokenReader(QIODevice* device);

    ~EdifactTokenReader();

    /**
     * pops the next token and returns it
     */
    Token next();

    /**
     * peeks the next token
     */
    Token peek();

    /**
     * true if peek or next is expected to return a valid token
     */
    bool hasNext();

    /**
     * error code from the parser. ErrorCodes::NoError is returned for no error
     */
    int errorCode() const;

private:
    const QScopedPointer<QtEdith::EdifactTokenReaderPrivate> d;

private:
    Q_DISABLE_COPY(EdifactTokenReader);
};

} // namespace

#endif // QTEDITH_EDIFACTTOKENREADER_H
